# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 17:00:47 2018

@author: pakozlow

Bunch separation
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib 
import seaborn as sns

plt.close('all')
data = np.load('excp1.npy')
data = np.array(data, dtype=np.float)
noOfSamples=2240
datamatrix = np.reshape(data,(10000,noOfSamples))
x = np.arange(0, 2240)

indexes_loop = [1] # ,312,7700

plt.figure('frame')
plt.clf()

plt.figure('bunches')
plt.clf()

colors = sns.color_palette()
index_color = 0

for index in indexes_loop:

    gx = datamatrix[index]

    ### Frame analysis 
    
    profileInputY = gx
    
    noiseFrameStart = 0
    noiseFrameEnd = 5
    
    # Defining a noise level          
    noiseLevel = 5.
        
    # Estimating noise amplitude and stop if there is no data (only noise) 
    sampledNoise = profileInputY[noiseFrameStart:noiseFrameEnd]
    noiseAmplitude = np.max(sampledNoise) - np.min(sampledNoise)
    
    # Readjusting the offset of profileInputY to put average noise to 0
    profileInputY -= np.mean(sampledNoise) 
    
    plt.figure('frame')
    plt.plot(profileInputY, color=colors[index_color])
    plt.hlines(noiseLevel*noiseAmplitude, 0, len(profileInputY), color=colors[index_color])
    
    
    index_color += 1

    # Locate the bunch indexes according to the noise
    bunchToNoiseLimit = np.max(profileInputY) / noiseLevel
        
    # Find the indexes of where the bunches are sitting
    indexesBunch = np.where(profileInputY > bunchToNoiseLimit)[0]
    integerArray = np.arange(len(indexesBunch))
    
    minimumBunchDistance = 10
    
    # Check how far the bunches are from each other and define bunch limits
    diffIndexesBunch = np.diff(indexesBunch)
    bunchDistancesIndex = diffIndexesBunch[diffIndexesBunch > minimumBunchDistance]
    
    indexesGap = np.where(diffIndexesBunch > minimumBunchDistance)[0]
    indexesGap = np.insert(indexesGap,0,-1)
    indexesGap = np.append(indexesGap,len(indexesBunch))
    
    # Update the number of bunches
    nBunches = np.sum(diffIndexesBunch > minimumBunchDistance) + 1
    
    fittingWindow = []
    for bunchNumber in range(nBunches):
        oneBunchFittingWindow = indexesBunch[(integerArray>=indexesGap[bunchNumber]+1)*(integerArray<=indexesGap[bunchNumber+1])]
        oneBunchFittingWindow = np.arange(oneBunchFittingWindow[0]-len(oneBunchFittingWindow), 
                                          oneBunchFittingWindow[-1]+len(oneBunchFittingWindow))
        fittingWindow.append(oneBunchFittingWindow)
    
        plt.figure('bunches')
        plt.plot(profileInputY[fittingWindow[bunchNumber]])
        
    
plt.show()
