from numpy import *
import numpy as np
import io as io
import matplotlib.pyplot as plt
from scipy import signal
import matplotlib.pylab as pylab
from scipy.optimize import fsolve
import scipy.integrate as integrate
import splitgenerator as sg
import compressRotateGenerator as crg

'''
Generate Bunch with splitting, rotation and batch compression
'''
NoOfShifts=5
shiftSize=0.25
mi=3
Tau_Init=4
bunchLen=1000
bunchFromTo = [-10,10]
finalListOfLastPositions=[]
FinalListOfZeros=[]
noOfRecords=14
'''
The data is generated in two attached files : splitgenerator and compressRotateGenerator.
3 first records is a simple beam profile so it has to be taken into consideration while creating a table of expected values.
The expected values are stored in  finalListOfLastPositions, and FinalListOfZeros.
'''
recordsSplit, lastTau, listOfPositions, listOfZeros = sg.generateSplitRecords(NoOfShifts,shiftSize,mi,Tau_Init, bunchLen, bunchFromTo)
zeros=[listOfZeros[(len(listOfZeros)-2)][0],
                listOfZeros[(len(listOfZeros)-2)][1],
                listOfZeros[(len(listOfZeros)-1)][0],
                listOfZeros[(len(listOfZeros)-1)][1]]
listOfLastPositions = listOfPositions[(len(listOfPositions)-2)][0],listOfPositions[(len(listOfPositions)-1)][0]

#0, 1st and 2nd sample will be a simple profile
simpleProfile= recordsSplit[0]
for x in range(0, 3):
    finalListOfLastPositions.append(listOfPositions[0][0])
    FinalListOfZeros.append(listOfZeros[0])

records = np.array([simpleProfile]*noOfRecords)

#3rd,4th,5th,6th,7th,8th will be different phases of split
records[2:7] = recordsSplit
#finalListOfLastPositions.append(listOfZeros[0])
#FinalListOfZeros.append(listOfPositions[0])
#coordinates of a 0 frame are added above, below only 1-4 records are handled
for x in range(1, 9):
    finalListOfLastPositions.append(listOfPositions[x][0])
    FinalListOfZeros.append(listOfZeros[x])

#duplicate the last frame after split
records[7:8] = recordsSplit[4]
finalListOfLastPositions.append(listOfPositions[len(listOfPositions)-2][0])
FinalListOfZeros.append(listOfZeros[len(listOfZeros)-2])
finalListOfLastPositions.append(listOfPositions[len(listOfPositions)-1][0])
FinalListOfZeros.append(listOfZeros[len(listOfZeros)-1])

#9- rotation of both bunches with different tau. In this case position doesn't change
divisionFactorOfTau=2
records[8],A1,A2,zeros=crg.bunchRotation(recordsSplit[4], mi, lastTau, divisionFactorOfTau, zeros, listOfLastPositions, bunchLen, bunchFromTo)
finalListOfLastPositions.append(listOfPositions[len(listOfPositions)-2][0])
finalListOfLastPositions.append(listOfPositions[len(listOfPositions)-1][0])
FinalListOfZeros.append([zeros[0],zeros[1]])
FinalListOfZeros.append([zeros[2],zeros[3]])

#9,10 - batch compression
lastTau=lastTau/divisionFactorOfTau
shift=0.10
records[9],listOfZeros, listOfPositions =crg.batchCompression(recordsSplit[4], mi, lastTau, shift, zeros, listOfLastPositions, bunchLen, bunchFromTo,A1,A2)
finalListOfLastPositions.append(listOfPositions[0])
finalListOfLastPositions.append(listOfPositions[1])
FinalListOfZeros.append([listOfZeros[0],listOfZeros[1]])
FinalListOfZeros.append([listOfZeros[2],listOfZeros[3]])
shift=shift+0.10
records[10],listOfZeros, listOfPositions=crg.batchCompression(recordsSplit[4], mi, lastTau, shift, zeros, listOfLastPositions, bunchLen, bunchFromTo,A1,A2)
finalListOfLastPositions.append(listOfPositions[0])
finalListOfLastPositions.append(listOfPositions[1])
FinalListOfZeros.append([listOfZeros[0],listOfZeros[1]])
FinalListOfZeros.append([listOfZeros[2],listOfZeros[3]])
shift=shift+0.10
records[11],listOfZeros, listOfPositions=crg.batchCompression(recordsSplit[4], mi, lastTau, shift, zeros, listOfLastPositions, bunchLen, bunchFromTo,A1,A2)
finalListOfLastPositions.append(listOfPositions[0])
finalListOfLastPositions.append(listOfPositions[1])
FinalListOfZeros.append([listOfZeros[0],listOfZeros[1]])
FinalListOfZeros.append([listOfZeros[2],listOfZeros[3]])
shift=shift+0.10
records[12],listOfZeros, listOfPositions=crg.batchCompression(recordsSplit[4], mi, lastTau, shift, zeros, listOfLastPositions, bunchLen, bunchFromTo,A1,A2)
finalListOfLastPositions.append(listOfPositions[0])
finalListOfLastPositions.append(listOfPositions[1])
FinalListOfZeros.append([listOfZeros[0],listOfZeros[1]])
FinalListOfZeros.append([listOfZeros[2],listOfZeros[3]])
shift=shift+0.15
records[13],listOfZeros, listOfPositions=crg.batchCompression(recordsSplit[4], mi, lastTau, shift, zeros, listOfLastPositions, bunchLen, bunchFromTo,A1,A2)
finalListOfLastPositions.append(listOfPositions[0])
finalListOfLastPositions.append(listOfPositions[1])
FinalListOfZeros.append([listOfZeros[0],listOfZeros[1]])
FinalListOfZeros.append([listOfZeros[2],listOfZeros[3]])

'''
Rescalling the x and y axis so that they will fit to no of samples(x) and a real case amplitude(y)
'''
finalListOfLastPositions=np.array(finalListOfLastPositions)#.flatten()
FinalListOfZeros=np.array(FinalListOfZeros)
#Shift x values by 1
for x in range(0, len(finalListOfLastPositions)):
    finalListOfLastPositions[x]=(finalListOfLastPositions[x])*50 + 500
    FinalListOfZeros[x]=(FinalListOfZeros[x])*50 + 500
#for x in range(0, len(records)):
#    records[x]=records[x]
t = np.linspace(bunchFromTo[0], bunchFromTo[1],  bunchLen, endpoint=False)
#Rescalling the amplitude
for x in range(0, len(records)):
    records[x]=records[x]*16383/2
'''
Digital number -> analog number. Assuming that max amplitude is 3.3V.
Implicit quantisation error.
'''
oneDivision=3.3/16383 # [V]
'''
Add noise - SNR=1/100
'''
amountOfNoise=100


snr  =  int(np.max(records)/amountOfNoise)
noise = np.random.normal(0,snr, t.shape)
for x in range(1, noOfRecords):
    records[x]=records[x]+noise
    rms = sqrt(mean(square(x)))
    
# the first frame has higher noise
amountOfNoise=50
snr  =  int(np.max(records)/amountOfNoise)
noise = np.random.normal(0,snr, t.shape)
records[0]=records[0]+noise
'''
Add attenuation distortion
'''
#decibel attenuation dBv = 20log10(Vout/Vin)
#so to reduce the signal by 2 dB (attenuation of cable) multiply the amplitude of each sample by:
attenuation=-1
#gain vout/vin => 
gain = 10 ** (attenuation / 10)
for x in range(0, noOfRecords):
    records[x]=records[x]*gain

'''
Move baseline up to avoid saturation
'''
for x in range(0, noOfRecords):
    records[x]=records[x] - min(records[x])

'''
plot
'''

for x in range(0, noOfRecords):
    pylab.plot(t,records[x])
pylab.show()
plt.figure()

#The table of values expected
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
i = ax.imshow(records, cmap="hot", aspect='auto',origin="lower")
ax.set_xlabel('No. of sample')
ax.set_ylabel('No. of frame')
fig.colorbar(i)
#pylab.show()
print(finalListOfLastPositions)
print(FinalListOfZeros)

'''
Store in the testbench recognizable format
'''

f = open("signalForTB.txt", "w+", encoding="utf-8")
#write noOfBunches
#write start and stop points of the bunches
f.write("%d %d\n" %(len(finalListOfLastPositions),noOfRecords*1000))

for position in finalListOfLastPositions:
     f.write("%d\n" % position)

for zero in FinalListOfZeros:
     f.write("%d %d\n" %(zero[0], zero[1]))

#writing waveform
for record in records:
    for sample in record:
     f.write("%d\n" % sample)
f.close()



'''
Generate simple bunch profile comb
'''
def simpleBunchComb():
    bunchLen=100
    noOfBunches=4
    distance=400
    baseline = 3000
    '''
    Bunches
    '''
    t = np.linspace(-1, 1,  bunchLen, endpoint=False)
    
    ## Gaussian signal from numpy package
    # i, q, e = signal.gausspulse(t, fc=5, retquad=True, retenv=True)
        
    ## gaussian signal from binomial form
    plt.figure()
    Tau=2
    mi=1.5
    e = (1 - 4*(t/Tau)**2)**mi
    plt.plot(e, '-')
    #
    plt.show()
    
    #real component, imaginary component, and envelope for a 5 Hz pulse, sampled at 100 Hz for 2 seconds:
    #plt.plot(t, i, t, q, t, e, '--')
    #start = int(bunchLen/2)
    #stop = int((2*bunchLen)-(bunchLen/2))
    #eCut=e[start:stop]
    
    #the baseline has to be moved a bit
    #max 16384
    eCut=e
    eCut=eCut*(16383 - baseline) #-4000 for safety
    #add baseline
    eCut=eCut + baseline
    #skewed hostogram
    plt.figure()
    plt.plot(eCut, '-')
    #
    plt.show()
    #inverted parabola
    
    '''
    Distance
    '''
    dist = [int(baseline)]*distance
    
    '''
    Timeline
    '''
    t = np.linspace(0, 2, bunchLen, endpoint=False)
    
    plt.plot(t, eCut, '--')
    
    plt.figure()
    
    '''
    Combination
    '''
    e2 = np.concatenate((dist,eCut,dist,eCut,dist,eCut,dist,eCut))
    #adding signal to noise
    amountOfNoise=200
    snr  =  int(np.max(e2/amountOfNoise))
    noise = np.random.normal(0,snr, e2.shape)
    #noise = noise + baseline
    #add noise and change the type to the one consistent with digitizer
    noise=noise.astype(int).astype(np.uint16)
    e2=e2.astype(int).astype(np.uint16)
    '''
    -White noise has equally random amounts of all frequencies
    -“Colored” noise has unequal amount for different frequencies
    -Since signals often have more low frequencies than high, the effect of
    white noise is usually greatest for high frequencies
    
    Therefore bunch shouldn't be covered with noise to the same
    '''
    e2 = e2 + noise
    
    
    #decibel attenuation dBv = 20log10(Vout/Vin)
    #so to reduce the signal by 2 dB (attenuation of cable) multiply the amplitude of each sample by:
    attenuation=-1
    #gain vout/vin => 
    gain = 10 ** (attenuation / 10)
    e2=e2*gain
    
    
    t2 = np.linspace(0, (noOfBunches*bunchLen) + (distance*noOfBunches), (noOfBunches*bunchLen) + (distance*noOfBunches), endpoint=False)
    plt.plot(t2, e2, '--')
    
    '''
    Output file
    '''
    
    f = open("signalForTB.txt", "w+", encoding="utf-8")
    #write noOfBunches
    #write start and stop points of the bunches
    f.write("%d " %noOfBunches)
    f.write("%d\n" %bunchLen)
    #writing waveform
    for i in e2:
         f.write("%d\n" % (i+1))
    f.close()
    
    
    '''
    Plot diff, gradient
    '''
    
    e2=e2.astype(int)
    noEl=int(noOfBunches*bunchLen) + (distance*noOfBunches)
    tempArray=np.array([0]*noEl)
    #gx = datamatrix[2700]
    tempArray[1:] = np.diff(e2)
    tempArray[0] = tempArray[1]
    
    diffSignal=tempArray
    gradientSignal = np.gradient(e2)
    
    #downsample
    #diffSignal=diffSignal[::5]
    #gradientSignal=gradientSignal[::5]
    
    plt.figure()
    plt.plot(diffSignal, '-')
    plt.xlabel('t [ns]')
    plt.ylabel('diff(Signal)')
    #
    plt.show()
    
    plt.figure()
    plt.plot(gradientSignal, '-')
    plt.xlabel('t [ns]')
    plt.ylabel('grad(Signal)')
    #
    plt.show()

'''
Store real data to testbench readable format
'''
def storeRealData():
    noOfSamples=2240
    noOfRecs=10000
    data = np.load('initialName0.npy')
    datamatrix = np.reshape(data,(noOfRecs,noOfSamples))
    wave =  np.concatenate((datamatrix[6250],datamatrix[6251]))
    wave=(wave+28920)
    wave = np.array(wave,dtype=uint16)
    plt.figure()
    plt.plot(wave, '-')
    plt.xlabel('t [ns]')
    plt.ylabel('grad(Signal)')
    plt.show()
    
    #set manually what is expected
    noOfBunches=4
    bunchLen=80
    
    f = open("signalForTB.txt", "w+", encoding="utf-8")
    #write noOfBunches
    #write start and stop points of the bunches
    f.write("%d " %noOfBunches)
    f.write("%d\n" %bunchLen)
    #writing waveform
    for i in wave:
         f.write("%d\n" % (i+1))
    f.close()

'''
Convert from tomoscope file into testbench readable format - real situation
'''
from numpy import *
import numpy as np
import io as io
import matplotlib.pyplot as plt
from scipy import signal

from io import StringIO  

def dataFromTomo():
    filename="/realData/Tomoscope/C2595allb_C40-77-78_MHFB_on_C80-88-89_BU4_3x3.5kV_4_1890E10ppp.dat"
    data = np.loadtxt(filename, delimiter="\n")
    
    dataRes=np.reshape(data,(100,8000))
    #shift baseline up
    baseline = np.min(dataRes)
    dataRes = -baseline+dataRes
    #normalize 0-1
    dataRes = dataRes/np.max(dataRes)
    #max range on 16 bits - 65536 
    dataRes = 65535 * dataRes# Now scale by 255
    data16 = dataRes.astype(np.uint16)
    
    #plot
    dataFinal = np.array(data16[0])
    plt.figure()
    plt.plot(dataFinal, '-')
    plt.xlabel('t [ns]')
    plt.ylabel('grad(Signal)')
    plt.show()
    
    #save
    f = open("signalForTB.txt", "w+", encoding="utf-8")
    #write noOfBunches
    #write start and stop points of the bunches
    f.write("%d " %noOfBunches)
    f.write("%d\n" %bunchLen)
    #writing waveform
    for i in dataFinal:
         f.write("%d\n" % (i+1))
    f.close()

def genWavelet():
     def ricker(f, length=256, dt=1):
         t = np.arange(-length/2, (length-dt)/2, dt)
         y = (1.0 - 2.0*(np.pi**2)*(f**2)*(t**2)) * np.exp(-(np.pi**2)*(f**2)*(t**2))
         return t, y
     f = 0.007 # A low wavelength of 25 Hz
     t, w = ricker(f)
     w=np.array(w*200, dtype=int)
     plt.plot(t, w)

#t = np.linspace(0, 2, 2 * 100, endpoint=False)
#
#t2 = np.concatenate((t, t))
#s2 = np.concatenate((e, e))
#
#plt.plot(t2, s2, '--')





#class SignalGenerator:
#
#    def __init__(self, name):
#        self.name = name
#        self.tricks = []    # creates a new empty list for each dog
#
#    def add_trick(self, trick):
#        self.tricks.append(trick)
#        
#        
#class Signal:
#
#    def __init__(self, name):
#        self.name = name
#        self.tricks = []    # creates a new empty list for each dog
#
#    def add_trick(self, trick):
#        self.tricks.append(trick)
#
#
