# -*- coding: utf-8 -*-
from numpy import *
import numpy as np
import io as io
import matplotlib.pyplot as plt
from scipy import signal
import matplotlib.pylab as pylab
from scipy.optimize import fsolve




# create a sample data

baseline = 3000


def generateBunch(size, mi=1.5):
    Tau=2
    size = int(size)
    distance=int((1000-size)/2)
    '''
    Bunches
    '''
    #sizeHalf = int(size/2)
    t = np.linspace(-1, 1,  size, endpoint=False)

    ## Gaussian signal from numpy package
    # i, q, e = signal.gausspulse(t, fc=5, retquad=True, retenv=True)

    ## gaussian signal from binomial form

    e = (1 - 4*(t/Tau)**2)**mi

    eCut=e
    eCut=eCut*(16383 - baseline) #-4000 for safety
    #add baseline
    eCut=eCut + baseline
    
    dist = [int(baseline)]*distance
    
    t = np.linspace(0, 2, size, endpoint=False)
    
    e2 = np.concatenate((dist,eCut,dist))
    #adding signal to noise
    amountOfNoise=200
    snr  =  int(np.max(e2/amountOfNoise))
    noise = np.random.normal(0,snr, e2.shape)
    #noise = noise + baseline
    #add noise and change the type to the one consistent with digitizer
    noise=noise.astype(int).astype(np.uint16)
    e2=e2.astype(int).astype(np.uint16)

    #e2 = e2 + noise
    #
    #attenuation=-1
    ##gain vout/vin => 
    #gain = 10 ** (attenuation / 10)
    #e2=e2*gain

    ##skewed hostogram
    #plt.figure()
    #plt.plot(e2, '-')
    ##
    #plt.show()

    e2 = np.concatenate((e2,[0])).astype(int).astype(np.uint16)

    return e2[0:1000]

inputSignal = np.array([np.array([0]*1000)]*150)
#expected values
inputSignalLengths = np.array([0]*150)
lent=41
for x in range(0, 25):
    lent=lent+3
    inputSignal[x] = generateBunch(lent)
    inputSignalLengths[x] = lent

for x in range(25, 50):
    lent=lent-4
    inputSignal[x] = generateBunch(lent)
    inputSignalLengths[x] = lent

for x in range(50, 75):
    lent=lent+16
    inputSignal[x] = generateBunch(lent)
    inputSignalLengths[x] = lent
    
for x in range(75, 100):
    lent=lent-15
    inputSignal[x] = generateBunch(lent)
    inputSignalLengths[x] = lent
    
z = np.linspace(-np.pi, np.pi, 25)
y = np.sin(z)*10

for x in range(100, 125):
    inputSignal[x] = generateBunch(lent + y[x-100]) 
    inputSignalLengths[x] = lent + y[x-100]

z = np.linspace(-np.pi, np.pi, 25)
y = np.sin(z)*30
for x in range(125, 150):
    inputSignal[x] = generateBunch(lent + y[x-125]) 
    inputSignalLengths[x] = lent + y[x-125]

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
#print(inputSignal)
i = ax.imshow(inputSignal, cmap="hot", aspect='auto',origin="lower")
ax.set_xlabel('No. of sample')
ax.set_ylabel('No. of frame')
fig.colorbar(i)

'''
Feedback mechanism
'''
#output matrix with minimas
outputMinMatrix = np.array([np.array([0]*1000)]*150)
withoutAdaptMatrix = [np.array([0]*1000)]*150
#a wavelet to convolve with
def ricker(f, length=500, dt=1):
     t = np.arange(-length/2, (length-dt)/2, dt)
     y = (1.0 - 2.0*(np.pi**2)*(f**2)*(t**2)) * np.exp(-(np.pi**2)*(f**2)*(t**2))
     return t, y

#Tr=200
#Td=Tr*(math.sqrt(3))
Td=50
f=math.sqrt(6)/(np.pi*Td)
#f = 0.007 # A low wavelength of 25 Hz
t, w = ricker(f)
w=np.array(w*200, dtype=int)
#sig=inputSignal[0]
#afterConv=np.convolve(sig ,w)
#tx=np.arange(0,1000)
#
##getting rid of teeth on both sides
#afterConvC=afterConv[250:1250]
#afterConvC[0:50]=[baseline]*50
#afterConvC[950:1000]=[baseline]*50
#plt.plot(tx, afterConvC)

withoutAdaptMatrix=np.array(inputSignal)

#results table
outputSignalLengthsNoFB = np.array([0]*150)
for x in range(0, 150):
    # convolute a turn with a wavelet based on a previous turn
    afterConv=np.convolve(inputSignal[x], w)
    afterConvC=afterConv[250:1250]
    indexMinLeft = int(np.where(afterConvC[0:500] == afterConvC[0:500].min())[0])
    indexMinRight = int(np.where(afterConvC[500:1000] == afterConvC[500:1000].min())[0])
    outputSignalLengthsNoFB[x]=indexMinRight+500-indexMinLeft
    print(indexMinLeft)
    #assign to outputMinMatrix
    outputMinMatrix[x][indexMinLeft]=1000
    outputMinMatrix[x][500+indexMinRight]=1000
    #asssign to withoutAdaptMatrix
    withoutAdaptMatrix[x][indexMinLeft]=0
    withoutAdaptMatrix[x][500+indexMinRight]=0
    
    
    # generate a wavelet for next turn (coefficients of the filter)
    
#fig, (ax1, ax2) = plt.subplots(ncols=2)
#fig = plt.figure()
#ax1 = fig.add_subplot(1, 1, 1)
##print(inputSignal)
#i1 = ax1.imshow(outputMinMatrix, cmap="hot", aspect='auto',origin="lower")
#ax1.set_xlabel('No. of sample')
#ax1.set_ylabel('No. of frame')
#fig.colorbar(i1)   

fig = plt.figure()
ax2 = fig.add_subplot(1, 1, 1)
#print(inputSignal)
i2 = ax2.imshow(withoutAdaptMatrix, cmap="hot", aspect='auto',origin="lower")
ax2.set_xlabel('No. of sample')
ax2.set_ylabel('No. of frame')
fig.colorbar(i2) 

'''
with adaptation
'''

withAdaptMatrix = [np.array([0]*1000)]*150

Td=50
f=math.sqrt(6)/(np.pi*Td)
#f = 0.007 # A low wavelength of 25 Hz
t, w = ricker(f)
w=np.array(w*300, dtype=int)
#sig=inputSignal[0]
#afterConv=np.convolve(sig ,w)
#tx=np.arange(0,1000)
#
##getting rid of teeth on both sides
#afterConvC=afterConv[250:1250]
##afterConvC[0:50]=[baseline]*50
##afterConvC[950:1000]=[baseline]*50
##plt.plot(tx, afterConvC)

withAdaptMatrix=np.array(inputSignal)
outputSignalLengthsFB = np.array([0]*150)
for x in range(0, 150):
    # convolute a turn with a wavelet based on a previous turn
    afterConv=np.convolve(inputSignal[x], w)
    afterConvC=afterConv[250:1250]
    indexMinLeft = int(np.where(afterConvC[0:500] == afterConvC[0:500].min())[0])
    indexMinRight = int(np.where(afterConvC[500:1000] == afterConvC[500:1000].min())[0])
    outputSignalLengthsFB[x] = indexMinRight+500-indexMinLeft
    feedbackLength=indexMinRight+500 - indexMinLeft
    
    Td=feedbackLength/4
    f=math.sqrt(6)/(np.pi*Td)
    t, w = ricker(f)
    w=np.array(w*200, dtype=int)

    #assign to outputMinMatrix
    #asssign to withoutAdaptMatrix
    withAdaptMatrix[x][indexMinLeft]=0
    withAdaptMatrix[x][500+indexMinRight]=0
    # generate a wavelet for next turn (coefficients of the filter)
print(afterConvC)

fig2 = plt.figure()
ax3 = fig2.add_subplot(1, 1, 1)
#print(inputSignal)
i3 = ax3.imshow(withAdaptMatrix, cmap="hot", aspect='auto',origin="lower")
ax3.set_xlabel('No. of sample')
ax3.set_ylabel('No. of frame')
fig2.colorbar(i3) 

'''
Error plot
'''

fpgaresultsCSV=numpy.loadtxt(open("measurementFPGAresults/fpgaresultsminus30.csv", "rb"), delimiter=",", skiprows=1)
supplement=fpgaresultsCSV[148]
fpgaresults=np.concatenate((fpgaresultsCSV, [supplement])).astype(int).astype(np.uint16)

fpgaresultsCSV=numpy.loadtxt(open("measurementFPGAresults/fpgaresults.csv", "rb"), delimiter=",", skiprows=1)
supplement=fpgaresultsCSV[148]
fpgaresultsNoStability=np.concatenate((fpgaresultsCSV, [supplement])).astype(int).astype(np.uint16)

fpgaresultsCSV=numpy.loadtxt(open("measurementFPGAresults/fpgaresultsrootthree.csv", "rb"), delimiter=",", skiprows=1)
supplement=fpgaresultsCSV[148]
fpgaresultsRootThree=np.concatenate((fpgaresultsCSV, [supplement])).astype(int).astype(np.uint16)

fpgaresultsCSV=numpy.loadtxt(open("measurementFPGAresults/fpgaresultsbankofFilters.csv", "rb"), delimiter=",", skiprows=1)
supplement=fpgaresultsCSV[148]
fpgaresultsBankFilters=np.concatenate((fpgaresultsCSV, [supplement])).astype(int).astype(np.uint16)

# expected values vs results from different algorithms/modules

plt.figure()
plt.subplot(111)
plt.plot(inputSignalLengths, label="Expected values")
plt.plot(outputSignalLengthsNoFB, label="no feedback (rigid value w=200), software implementation")
plt.plot(outputSignalLengthsFB, label="feedback (starts with w=350), software implementation")
plt.plot(fpgaresults, label="FPGA implementation with feedback (w=w[x-20])")
plt.plot(fpgaresultsNoStability, label="FPGA implementation with feedback (w=w[x])")
plt.plot(fpgaresultsRootThree, label="FPGA implementation with feedback (w=w[x/root(3)])")
plt.plot(fpgaresultsBankFilters, label="FPGA implementation with bank of filters")
plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
       ncol=2, mode="expand", borderaxespad=0.)
plt.grid(True)
plt.legend(loc=9, bbox_to_anchor=(0.5, -0.1), prop={'size': 13}, ncol=2)
plt.xlabel("time [No. of frame]", fontsize=13)
plt.ylabel("size [No. of samples]", fontsize=13)
#plt.tight_layout()

# Absolute Error

plt.figure()
plt.subplot(111)
plt.plot(outputSignalLengthsNoFB-inputSignalLengths, label="No feedback (rigid value w=200), software implementation")
plt.plot(outputSignalLengthsFB-inputSignalLengths, label="feedback (starts with w=350), software implementation")
plt.plot(fpgaresults-inputSignalLengths, label="FPGA implementation with feedback (w=w[x-20])")
plt.plot(fpgaresultsNoStability-inputSignalLengths, label="FPGA implementation with feedback (w=w[x])")
plt.plot(fpgaresultsRootThree-inputSignalLengths, label="FPGA implementation with feedback (w=w[x/root(3)])")
plt.plot(fpgaresultsBankFilters-inputSignalLengths, label="FPGA implementation with bank of filters")
plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
       ncol=2, mode="expand", borderaxespad=0.)
plt.grid(True)
plt.legend(loc=9, bbox_to_anchor=(0.5, -0.1), prop={'size': 13}, ncol=2)
plt.xlabel("time [No. of frame]", fontsize=13)
plt.ylabel("error (size [No. of samples])", fontsize=13)

# Relative error (ER = | 1 - (Vapprox / V) |)

plt.figure()
plt.subplot(111)
plt.plot((1 - outputSignalLengthsNoFB/inputSignalLengths)*100, label="No feedback (rigid value w=200), software implementation")
plt.plot((1 - outputSignalLengthsFB/inputSignalLengths)*100, label="feedback (starts with w=350), software implementation")
plt.plot((1 - fpgaresults/inputSignalLengths)*100, label="FPGA implementation with feedback (w=w[x-20])")
plt.plot((1 - fpgaresultsNoStability/inputSignalLengths)*100, label="FPGA implementation with feedback (w=w[x])")
plt.plot((1 - fpgaresultsRootThree/inputSignalLengths)*100, label="FPGA implementation with feedback (w=w[x/root(3)])")
plt.plot((1 - fpgaresultsBankFilters/inputSignalLengths)*100, label="FPGA implementation with bank of filters")
plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
       ncol=2, mode="expand", borderaxespad=0.)
plt.grid(True)
plt.legend(loc=9, bbox_to_anchor=(0.5, -0.1), prop={'size': 13}, ncol=2)
plt.xlabel("time [No. of frame]", fontsize=13)
plt.ylabel("Relative error [%]", fontsize=13)


'''
write
'''

#scalledSignal = (inputSignal/1.4)+3000
#inputSignal16bit=scalledSignal.astype(int).astype(np.uint16)
#
#f = open("generatedTest.txt", "w+", encoding="utf-8")
##write noOfBunches
##write start and stop points of the bunches
#f.write("1 ")
#f.write("1\n")
##writing waveform
#for i in inputSignal16bit:
#    for x in i :
#     f.write("%d\n" %x)
#f.close()


'''
no Fp arithmetic
TD<b
a=pi*100=314
b=sqrt(6)/pi * 1000 = 780
'''
def ricker(Td, length=100):
     f=(sqrt(6))/(np.pi*Td)
     fromto = int(floor(length/2))
     y = np.array([0.0]*length)
     for t in range(-fromto, fromto):
#         y[t+49] = (1/(10**4)) * (1/(10**3)) * (10**3 - (1199716128/(Td**2)) * (1/(10**5)) * t**2 )  * \
#         ( 10**4    +     (599858064/(Td**2) * (1/(10**4)) * t**2 )    +    ((599858064 * (1/(10**6)) *t**2)/(Td**2))/2 + \
#         ((10 * ( ( 599858064 * (1/(10**7)) * t**2 ) / (Td**2) ))**3)/6 + (( ( 599858064 * (1/(10**7)) * t**2 ) / (Td**2) )**4)/24)
         y[t+fromto]=(1.0 - 2.0*(np.pi**2)*(f**2)*(t**2)) * np.exp(-(np.pi**2)*(f**2)*(t**2))
     return y

#creating a look-up table for wavelet coefficients
fromTd=0
ToTd=230
noOfWavel=ToTd-fromTd
arrayOfCorrespondingTd = np.array([0]*noOfWavel)
arrayOfCoefficients = np.array([np.array([0]*512)]*(noOfWavel))
for x in range(fromTd+1, ToTd):
    arrayOfCoefficients[x-fromTd] = (ricker(x,512)*100).astype(int)
    arrayOfCorrespondingTd[x-fromTd] = x
    
fig2 = plt.figure()
ax3 = fig2.add_subplot(1, 1, 1)
#print(inputSignal)
i3 = ax3.imshow(arrayOfCoefficients, cmap="hot", aspect='auto',origin="lower")
ax3.set_xlabel('No. of sample')
ax3.set_ylabel('No. of frame')
fig2.colorbar(i3)  


#save
f = open("coefficentsRAM.txt", "w+", encoding="utf-8")
#write noOfBunches
#write start and stop points of the bunches
f.write("%d\n" %noOfWavel)
#writing waveform
for x in arrayOfCoefficients:
    for i in x:
        f.write("%d\n" % (i+1))
f.close()




'''
tesbench with splitting
'''
from scipy import signal
import matplotlib.pylab as pylab
from scipy.optimize import fsolve
import scipy.integrate as integrate
import splitgenerator as sg
import compressRotateGenerator as crg

'''
Generate Bunch with splitting
'''
noOfRecords=200
NoOfShifts=noOfRecords
shiftSize=0.01
mi=3
Tau_Init=2
bunchLen=1000
bunchFromTo = [-10,10]
finalListOfLastPositions=[]
FinalListOfZeros=[]

'''
The data is generated in two attached files : splitgenerator and compressRotateGenerator.
3 first records is a simple beam profile so it has to be taken into consideration while creating a table of expected values.
The expected values are stored in  finalListOfLastPositions, and FinalListOfZeros.
'''
recordsSplit, lastTau, listOfPositions, listOfZeros = sg.generateSplitRecords(NoOfShifts,shiftSize,mi,Tau_Init, bunchLen, bunchFromTo)
zeros=[listOfZeros[(len(listOfZeros)-2)][0],
                listOfZeros[(len(listOfZeros)-2)][1],
                listOfZeros[(len(listOfZeros)-1)][0],
                listOfZeros[(len(listOfZeros)-1)][1]]
listOfLastPositions = listOfPositions[(len(listOfPositions)-2)][0],listOfPositions[(len(listOfPositions)-1)][0]


records = np.array([simpleProfile]*noOfRecords)

#3rd,4th,5th,6th,7th,8th will be different phases of split
records[0:noOfRecords] = recordsSplit

records=(records*12000)+3000
records=records.astype(int).astype(np.uint16)

#calculating the expected lengths
lengthsarray=np.array([0]*noOfRecords)
lengthsarray[0]=(listOfZeros[0][1] - listOfZeros[0][0])*50

def my_range(start, end, step):
    while start <= end:
        yield start
        start += step

index=1
for x in my_range(1, (noOfRecords*2)-2, 2):
    lengthsarray[index]=(listOfZeros[x][1] - listOfZeros[x][0])*50
    index=index+1


fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
i = ax.imshow(records, cmap="hot", aspect='auto',origin="lower")
ax.set_xlabel('No. of sample')
ax.set_ylabel('No. of frame')
fig.colorbar(i)


#plot comparison with FPGA FB-based results

fpgaresultsCSV=numpy.loadtxt(open("measurementFPGAresults/fpgaresultssplitFB_200sampl.csv", "rb"), delimiter=",", skiprows=1)

fpgaresultsFBSplit=fpgaresultsCSV.astype(int).astype(np.uint16)

plt.figure()
plt.subplot(211)
plt.plot(lengthsarray, label="Expected values")
plt.plot(fpgaresultsFBSplit, label="FPGA FB Adaptive")
plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
       ncol=2, mode="expand", borderaxespad=0.)




#save
f = open("splitGenData.txt", "w+", encoding="utf-8")
#write noOfBunches
#write start and stop points of the bunches
f.write("1 ")
f.write("1\n")
#writing waveform
for i in records:
    for x in i :
     f.write("%d\n" %x)
f.close()


'''
real data
'''
#filename="/realData/Tomoscope/C2595allb_C40-77-78_MHFB_on_C80-88-89_BU4_3x3.5kV_4_1890E10ppp.dat"
#data = np.loadtxt(filename, delimiter="\n")
#
#dataRes=np.reshape(data,(100,8000))
##shift baseline up
#baseline = np.min(dataRes)
#dataRes = -baseline+dataRes
##normalize 0-1
#dataRes = dataRes/np.max(dataRes)
##max range on 16 bits - 65536 
#dataRes = 65535 * dataRes# Now scale by 255
#data16 = dataRes.astype(np.uint16)
#
##plot
#dataFinal = np.array(data16[0])
#plt.figure()
#plt.plot(dataFinal, '-')
#plt.xlabel('t [ns]')
#plt.ylabel('grad(Signal)')
#plt.show()

#without scalling

# General imports
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize, curve_fit
import warnings
# from _warnings import warn

# Cedar imports
from cedarFit import profilefunctions
from cedarFit.options import FitOptions
from cedarFit.fit import *

#read real data
a = np.load("onebunchTest.npy")
plt.figure()
plt.imshow(a)
plt.grid(True)

t = np.linspace(0, 1120, num=1120).astype(int)

aT = np.load("onebunchTest.npy").T
fittingResults=np.array([0]*1325)
for index in range(0, 1325):
    bunchPosition, bunchLength, extraParameters = parabolicLineFit(t,a[index])
    fittingResults[index]=bunchLength

fpgaresultsCSV = numpy.loadtxt(open("measurementFPGAresults/fpgaresultsrealData.csv", "rb"), delimiter=",", skiprows=1)

fpgaresultsCSV = np.concatenate(([0]*19,fpgaresultsCSV)).astype(int).astype(np.uint16)

fpgaresultsFBRealData=fpgaresultsCSV.astype(int).astype(np.uint16)

#with compensation
#fpgaresultsFBRealData=(fpgaresultsFBRealData*0.87)[2:1325]

plt.figure()
plt.subplot(111)
plt.plot(fittingResults, label="results of parabola fitting")
plt.plot(fpgaresultsFBRealData, label="FPGA FB Adaptive")
plt.grid(True)
plt.legend(loc=9, bbox_to_anchor=(0.5, -0.1), prop={'size': 13}, ncol=2)
plt.xlabel("time [No. of frame]", fontsize=13)
plt.ylabel("size [No. of samples]", fontsize=13)


"""
dependency of size to a relative error
"""
inputSignal = np.array([np.array([0]*1000)]*150)
#expected values
inputSignalLengths = np.array([0]*150)
lent=40
for x in range(0, 150):
    inputSignal[x] = generateBunch(lent)
    inputSignalLengths[x] = lent
    lent=lent+1
plt.figure()
plt.imshow(inputSignal)


inputSignalLengthsi = [i for i in range(0, len(inputSignalLengths))]

#save
#f = open("dependencyToSize.txt", "w+", encoding="utf-8")
##write noOfBunches
##write start and stop points of the bunches
#f.write("1 ")
#f.write("1\n")
##writing waveform
#for i in inputSignal:
#    for x in i :
#     f.write("%d\n" %x)
#f.close()

fpgaresultsSizDepCSV = numpy.loadtxt(open("measurementFPGAresults/fpgaresultsDependencySize.csv", "rb"), delimiter=",", skiprows=1)


fig, ax = plt.subplots()
plt.plot(inputSignalLengths, label="expected values")
plt.plot(fpgaresultsSizDepCSV, label="FPGA FB Adaptive")

plt.xticks(inputSignalLengthsi,inputSignalLengths.astype(int))
n = 20  # Keeps every 7th label
[l.set_visible(False) for (i,l) in enumerate(ax.xaxis.get_ticklabels()) if i % n != 0]



plt.legend(loc=9, bbox_to_anchor=(0.5, -0.1), prop={'size': 13}, ncol=2)
plt.xlabel("time [No. of frame] / input Tau[No. of samples]", fontsize=13)
plt.ylabel("size [No. of samples]", fontsize=13)

#error
fig, ax = plt.subplots()
plt.plot(np.abs(inputSignalLengths[0:149]-fpgaresultsSizDepCSV), label="Error (expected and resulting from FPGA analysis lengths [No. of samples])")


plt.xticks(inputSignalLengthsi,inputSignalLengths.astype(int))
n = 20  # Keeps every 7th label
[l.set_visible(False) for (i,l) in enumerate(ax.xaxis.get_ticklabels()) if i % n != 0]



plt.legend(loc=9, bbox_to_anchor=(0.5, -0.1), prop={'size': 13}, ncol=2)
plt.xlabel("time [No. of frame] / input Tau[No. of samples]", fontsize=13)
plt.ylabel("size [No. of samples]", fontsize=13)




"""
dependency of mu on relative error
"""
import matplotlib.ticker as ticker
inputSignal = np.array([np.array([0]*1000)]*150)
#expected values
inputSignalLengths = np.array([0]*150)
inputMiLengths = np.array([0.0]*150)
lent=70
mi=1.0
for x in range(0, 150):
    inputSignal[x] = generateBunch(lent, mi)
    inputSignalLengths[x] = lent
    mi=mi+0.05
    inputMiLengths[x]=mi
plt.figure()
plt.imshow(inputSignal)

    
#save
#f = open("dependencyToMi.txt", "w+", encoding="utf-8")
##write noOfBunches
##write start and stop points of the bunches
#f.write("1 ")
#f.write("1\n")
##writing waveform
#for i in inputSignal:
#    for x in i :
#     f.write("%d\n" %x)
#f.close()

fpgaresultsMiDepCSV = numpy.loadtxt(open("measurementFPGAresults/fpgaresultsDependencyMi.csv", "rb"), delimiter=",", skiprows=1)

fpgaresultsMiDepCSV = np.concatenate(([fpgaresultsMiDepCSV[0]],fpgaresultsMiDepCSV)).astype(int).astype(np.uint16)


# create an index for each tick position
inputMiLengthsi = [i for i in range(0, len(inputMiLengths))]

#plt.figure()
fig, ax = plt.subplots()
#plt.subplot(111)
plt.plot(inputMiLengthsi, inputSignalLengths, label="expected values")
plt.plot(inputMiLengthsi, fpgaresultsMiDepCSV, label="FPGA FB Adaptive")


plt.xticks(inputMiLengthsi,inputMiLengths.astype(int))
n = 20  # Keeps every 7th label
[l.set_visible(False) for (i,l) in enumerate(ax.xaxis.get_ticklabels()) if i % n != 0]

#plt.grid(True)
plt.legend(loc=9, bbox_to_anchor=(0.5, -0.1), prop={'size': 13}, ncol=2)
plt.xlabel("mu ", fontsize=13)
plt.ylabel("size of profile [No. of samples]", fontsize=13)

#error
fig, ax = plt.subplots()
plt.plot(np.abs(inputSignalLengths-fpgaresultsMiDepCSV), label="Error (expected and resulting from FPGA analysis lengths [No. of samples])")


plt.xticks(inputSignalLengthsi,inputSignalLengths.astype(int))
n = 20  # Keeps every 7th label
[l.set_visible(False) for (i,l) in enumerate(ax.xaxis.get_ticklabels()) if i % n != 0]



plt.legend(loc=9, bbox_to_anchor=(0.5, -0.1), prop={'size': 13}, ncol=2)
plt.xlabel("time / input $\mu$ [No. of samples]", fontsize=13)
plt.ylabel("size [No. of samples]", fontsize=13)

