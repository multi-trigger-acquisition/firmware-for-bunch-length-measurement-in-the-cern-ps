# -*- coding: utf-8 -*-
"""
Created on Mon Oct 29 13:34:33 2018

@author: pakozlow

Live plotting of bunch length from offline data. 
The gui used in this case is PyQTGraph.

"""

# Import libraries
from numpy import *
import numpy as np
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
import random

# Create object serial port
#portName = "COM12"                      # replace this port name by yours!
#baudrate = 9600
#ser = serial.Serial(portName,baudrate)

noOfPlots=84

ArrayOfPlots = np.array([None]*noOfPlots)
ArrayOfCurves = np.array([None]*noOfPlots)
ValueToAdd = np.array([0]*noOfPlots)
#array of pointers to positions
PtrArr = np.array([0]*noOfPlots)
#array of data
XmData = np.array([None]*noOfPlots)


#create 

import numpy as np
#the data are stored in packs of 10000 records with 2240 samples
data = np.load('excp1.npy')
noOfSamples=2240
nofRec = 10000
datamatrix = np.reshape(data,(nofRec,noOfSamples))


### START QtApp #####
app = QtGui.QApplication([])            # you MUST do this once (initialize things)
####################

win = pg.GraphicsWindow(title="Longitudinal bunch length") # creates a window

windowWidth = 500                       # width of the window displaying the curve
#Xm = linspace(0,0,windowWidth)          # create array that will contain the relevant time series     
#ptr = -windowWidth                      # set first x position

plot = win.addPlot(title="Bunches",enableMenu=False,name='Longitudinal bunch length') # creates empty space for the plot in the window
plot.setLabels(left = ('Length','units'))
#plot.disableAutoRange(axis=1)

for x in range(0, noOfPlots):
    ArrayOfCurves[x] = plot.plot(pen=(x,noOfPlots)) # create an empty "plot" (a curve to plot)
    PtrArr[x] = -windowWidth
    XmData[x] = linspace(0,0,windowWidth)



turnCounter=0


tempArray = np.array([0]*noOfSamples)

def CalcArrOfLen(Arr):
    gx=Arr
    #gx = datamatrix[2700]
    tempArray[1:] = np.diff(gx)
    tempArray[0] = tempArray[1]
    gx=tempArray
    
    
    minV = np.min(gx)
    maxV = np.max(gx)
    ptp = maxV-minV
    levelOfintersectionUp = 150# int(minV + 0.25*ptp)
    levelOfintersectionDown =-150 # int(maxV - 0.25*ptp)
    
    #gx[(gx > levelOfintersectionUp) & (gx < levelOfintersectionDown)] = 0
    
    thoseUp = (gx < levelOfintersectionUp) 
    thoseDown = (gx > levelOfintersectionDown)
    
    
    listOfBunches=[]
    
    
    #state 0 = zero level, 0/1 - first up, 1/2 second up, 2/3 first down, 3/0 second down, back to 0
    state = 0
    
    first=None
    last=None
    
    '''
    False is True in this case
    '''
    
    #is the bunch to be ignored?
    ignore=False
    #check what is on the beginning
    if(thoseUp[0]==False):
        #this is upper diff
        ignore=True
        state=2
    elif(thoseDown[0]==False):
        #this is lower diff
        ignore=True
        state=3
    
    
    #
    ##optimisation by embedding one if on another
    #Finite state machine
    NoEl=1
    while NoEl < noOfSamples:
        if(state==0):
            if(thoseUp[NoEl]==False):
                #print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                first=NoEl
                #number of samples to ignore
                NoEl=NoEl+15
                state=1
        elif(state==1):
            if(thoseUp[NoEl]==True):
                #print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                #number of samples to ignore
                NoEl=NoEl+15
                state=2
        elif(state==2):
            if(thoseDown[NoEl]==False):
                #number of samples to ignore
                #print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                NoEl=NoEl+15
                state=3
        elif(state==3):
            if(thoseDown[NoEl]==True):
                #number of samples to ignore
                #print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                last=NoEl
                if(ignore==False):
                    #add to list
                    listOfBunches.append([first,last]) 
                else:
                    ignore=False
                state=0
                NoEl=NoEl+15
        #always increment by one        
        NoEl=NoEl+1
        
    return listOfBunches


# Realtime data plot. Each time this function is called, the data display is updated
def update():
    global ArrayOfCurves, PtrArr, XmData, turnCounter
    
    # array lengths for the turn
    listOfPoints = CalcArrOfLen(datamatrix[turnCounter])
    
    # counterid
    counterIdx = 0
    
    #updateplots for each turn
    for x in range(0, noOfPlots):
        XmData[x][:-1] = XmData[x][1:]                      # shift data in the temporal mean 1 sample left
        #print(listOfPoints)
        if(counterIdx+2<len(listOfPoints)):
            value = listOfPoints[counterIdx][1] - listOfPoints[counterIdx][0]              # calculate lengths by taking 2 consecutive points distance
            counterIdx+=2
        else:
            value= 0
        XmData[x][-1] = float(value)                 # vector containing the instantaneous values
        #print(XmData[x][-1])
        PtrArr[x] += 1                              # update x position for displaying the curve
    
        ArrayOfCurves[x].setData(XmData[x])                     # set the curve with this data
        #ArrayOfCurves[x].setPos(PtrArr[x],0)                   # set x position in the graph to 0
        
    # Increase the iuncrement value in order to increase performance
    turnCounter=turnCounter+1
    if(turnCounter%100==0):
        QtGui.QApplication.processEvents() 

### MAIN PROGRAM #####    
# this is a brutal infinite loop calling your realtime data plot
while True: update()

### END QtApp ####
pg.QtGui.QApplication.exec_() # you MUST put this at the end
##################