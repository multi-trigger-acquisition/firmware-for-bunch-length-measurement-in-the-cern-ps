# -*- coding: utf-8 -*-
"""
Created on Mon Oct 29 13:34:33 2018

@author: pakozlow

Derivative based, greedy approach
"""

# Import libraries
from numpy import *
import numpy as np
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
import random

# Create object serial port
#portName = "COM12"                      # replace this port name by yours!
#baudrate = 9600
#ser = serial.Serial(portName,baudrate)

noOfPlots=84

ArrayOfPlots = np.array([None]*noOfPlots)
ArrayOfCurves = np.array([None]*noOfPlots)
ValueToAdd = np.array([0]*noOfPlots)
#array of pointers to positions
PtrArr = np.array([0]*noOfPlots)
#array of data
XmData = np.array([None]*noOfPlots)


#create 

import numpy as np
#the data are stored in packs of 10000 records with 2240 samples
data = np.load('excp1.npy')
noOfSamples=2240
nofRec = 10000
datamatrix = np.reshape(data,(nofRec,noOfSamples))


### START QtApp #####
app = QtGui.QApplication([])            # you MUST do this once (initialize things)
####################

win = pg.GraphicsWindow(title="Longitudinal bunch length") # creates a window

windowWidth = 500                       # width of the window displaying the curve
#Xm = linspace(0,0,windowWidth)          # create array that will contain the relevant time series     
#ptr = -windowWidth                      # set first x position

plot = win.addPlot(title="Bunches") # creates empty space for the plot in the window
plot.setLabels(left = ('Length','units'))
for x in range(0, noOfPlots):
    ArrayOfCurves[x] = plot.plot(pen=(x,noOfPlots)) # create an empty "plot" (a curve to plot)
    PtrArr[x] = -windowWidth
    XmData[x] = linspace(0,0,windowWidth)



turnCounter=0

'''
import numpy as np
import matplotlib.pyplot as plt
import matplotlib 

plt.close('all')
noOfSamples=2240
tempArray = np.array([0]*noOfSamples)

data = np.load('initialName0.npy')

datamatrix = np.reshape(data,(10000,noOfSamples))
x = np.arange(0, 2240)

gx = datamatrix[2700]
tempArray[1:] = np.diff(gx)
tempArray[0]=tempArray[1]
gx=tempArray


minV = np.min(gx)
maxV = np.max(gx)
ptp = maxV-minV
levelOfintersectionUp = int(minV + 0.35*ptp)
levelOfintersectionDown = int(maxV - 0.35*ptp)

#gx[(gx > levelOfintersectionUp) & (gx < levelOfintersectionDown)] = 0

#thoseUp = (gx > levelOfintersectionUp) 
#thoseDown = (gx < levelOfintersectionDown)


listOfBunches=[]


#state 0 = zero level, 1 - first up, 2 second up, 3 first down, 4 second down, back to 0
state = None

first=None
last=None

#is the bunch to be ignored?
ignore=False
#check what is on the beginning
if(thoseUp[0]=True):
    #this is upper diff
    ignore=True
    state=3
elif(thoseDown[0]=True):
    #this is lower diff
    ignore=True
    state=4



#optimisation by embedding one if on another
#Finite state machine
NoEl=1
while NoEl < noOfSamples:
    if(state==0):
        if(thoseUp[NoEl]=True):
            first=NoEl
            #number of samples to ignore
            NoEl=NoEl+9
            state=1
    if(state==1):
        if(thoseUp[NoEl]=True):
            #number of samples to ignore
            NoEl=NoEl+9
            state=2
    if(state==3):
        if(thoseDown[NoEl]=True):
            #number of samples to ignore
            NoEl=NoEl+9
            state=3
    if(state==4):
        if(thoseDown[NoEl]=True):
            #number of samples to ignore
            NoEl=NoEl+9
            second=NoEl
            if(ignore==False):
                #add to list
                listOfBunches.append([first,last]) 
            else:
                ignore==False
            state=0
    #always increment by one        
    NoEl=NoEl+9
    

plt.plot(x, gx, '-')



idxUp = np.argwhere(np.diff(np.sign(levelOfintersectionUp - g))).flatten()
idxDown = np.argwhere(np.diff(np.sign(levelOfintersectionDown - g))).flatten()



plt.plot(x[idxUp], fUp[idxUp], 'ro')
plt.plot(x[idxDown], fDown[idxDown], 'ro')
plt.show()
plt.figure()
plt.imshow(datamatrix, interpolation='nearest', cmap=plt.cm.ocean, aspect='auto', origin ='bottom')
plt.show()
'''
tempArray = np.array([0]*noOfSamples)

# Realtime data plot. Each time this function is called, the data display is updated
def update():
    global ArrayOfCurves, PtrArr, XmData, turnCounter
    
    
    gx = datamatrix[turnCounter]
    tempArray[1:] = np.diff(gx)
    tempArray[0]=tempArray[1]
    
    #taking derivative
    
    
    
    
    #find intersect points (indices of them)
    #find threshold
    minV = np.min(datamatrix[turnCounter])
    maxV = np.max(datamatrix[turnCounter])
    ptp = maxV-minV
    levelOfintersection = minV + 0.15*ptp
       
    
    
    if(turnCounter<nofRec):
        g = datamatrix[turnCounter]
    else:
        g = [0]*noOfSamples
    idx = np.argwhere(np.diff(np.sign(levelOfintersection - g))).flatten()
    
    '''
    filter wrong results
    '''
    if(datamatrix[turnCounter][0]>levelOfintersection):
        #filter if bunch is incomplete on the begining
        idx=idx[1:]
    noOfEl=len(datamatrix[turnCounter])
    if(datamatrix[turnCounter][noOfEl-1]>levelOfintersection):
        idx=idx[:len(idx)-1]
    #filter out noisy turns
    #sd = datamatrix[turnCounter][:6].std()
    if(ptp<1300):
        arr=[]
        idx=np.array(arr)
    
    #plt.plot(x[idx], f[idx], 'ro')
    counterIdx = 0
    #print(idx)
    
    #updateplots
    for x in range(0, noOfPlots):
        XmData[x][:-1] = XmData[x][1:]                      # shift data in the temporal mean 1 sample left
        if(counterIdx+2<len(idx)):
            value = idx[counterIdx+1] - idx[counterIdx]              # read line (single value) from the serial port
            counterIdx+=2
        else:
            value= 0
        XmData[x][-1] = float(value)                 # vector containing the instantaneous values
        #print(XmData[x][-1])
        PtrArr[x] += 1                              # update x position for displaying the curve
    
        ArrayOfCurves[x].setData(XmData[x])                     # set the curve with this data
        ArrayOfCurves[x].setPos(PtrArr[x],0)                   # set x position in the graph to 0
        
    turnCounter=turnCounter+2
    QtGui.QApplication.processEvents()    # you MUST process the plot now

### MAIN PROGRAM #####    
# this is a brutal infinite loop calling your realtime data plot
while True: update()

### END QtApp ####
pg.QtGui.QApplication.exec_() # you MUST put this at the end
##################