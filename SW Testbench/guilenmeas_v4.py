# -*- coding: utf-8 -*-
"""
Created on Thu Nov  1 11:44:36 2018

@author: pakozlow
This is not a live visualisation, just a 3d and 2d plot.

3d gradient plot with edge detection - comparison filtered/non filtered

The detection is based on taking first intersection point with upper threshold with
second intersection point with lower threshold of derivative of the original signal.
The approach is more reliable because it is based on FSM. NEvertheless some glitches are appearing.

"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib 

plt.close('all')

'''
Gradient and 2d plot for non-filtered version
'''

noOfSamples=2240
noOfRecs=10000
tempArray = np.array([0]*noOfSamples)

data = np.load('initialName0.npy')

datamatrix = np.reshape(data,(noOfRecs,noOfSamples))
pointsmatrix = np.array([None]*noOfRecs)
x = np.arange(0, 2240)

def CalcArrOfLen(Arr):
    gx=Arr
    #gx = datamatrix[2700]
    tempArray[1:] = np.diff(gx)
    tempArray[0] = tempArray[1]
    gx=tempArray
    
    
    minV = np.min(gx)
    maxV = np.max(gx)
    ptp = maxV-minV
    levelOfintersectionUp = 150# int(minV + 0.25*ptp)
    levelOfintersectionDown =-150 # int(maxV - 0.25*ptp)
    
    #gx[(gx > levelOfintersectionUp) & (gx < levelOfintersectionDown)] = 0
    
    thoseUp = (gx < levelOfintersectionUp) 
    thoseDown = (gx > levelOfintersectionDown)
    
    
    listOfBunches=[]
    
    
    #state 0 = zero level, 0/1 - first up, 1/2 second up, 2/3 first down, 3/0 second down, back to 0
    state = 0
    
    first=None
    last=None
    
    #is the bunch to be ignored?
    ignore=False
    #check what is on the beginning
    if(thoseUp[0]==False):
        #this is upper diff
        ignore=True
        state=2
    elif(thoseDown[0]==False):
        #this is lower diff
        ignore=True
        state=3
    
    
    #
    ##optimisation by embedding one if on another
    #Finite state machine
    NoEl=1
    while NoEl < noOfSamples:
        if(state==0):
            if(thoseUp[NoEl]==False):
                #print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                first=NoEl
                #number of samples to ignore
                NoEl=NoEl+15
                state=1
        elif(state==1):
            if(thoseUp[NoEl]==True):
                #print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                #number of samples to ignore
                NoEl=NoEl+15
                state=2
        elif(state==2):
            if(thoseDown[NoEl]==False):
                #number of samples to ignore
                #print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                NoEl=NoEl+15
                state=3
        elif(state==3):
            if(thoseDown[NoEl]==True):
                #number of samples to ignore
                #print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                last=NoEl
                if(ignore==False):
                    #add to list
                    listOfBunches.append([first,last]) 
                else:
                    ignore=False
                state=0
                NoEl=NoEl+15
        #always increment by one        
        NoEl=NoEl+1
        
    return listOfBunches
#
    

#datamatrix = np.reshape(data,(noOfRecs,noOfSamples))
#pointsmatrix 
#arr = np.arange(100, dtype=float).reshape(10, 10)
#arr[~(arr % 7).astype(bool)] = np.nan

datamatrixFloat = np.array(datamatrix, dtype=float)
cnt=0
for turn in datamatrix:
    #convert to numpy
    pointsmatrix[cnt]=np.array(CalcArrOfLen(turn), dtype=int)
    recordPoints = np.array(pointsmatrix[cnt])
    datamatrixFloat[cnt,recordPoints]=np.nan
#    for point in pointsmatrix:
#        datamatrix[pointsmatrix[point].astype(bool)] = np.nan
    #pointsmatrix[cnt]=np.array(CalcArrOfLen(turn), dtype=int)
    cnt+=1

#plt.close('all')
plt.figure()
current_cmap = matplotlib.cm.get_cmap()
current_cmap.set_bad(color='red')
x = np.arange(noOfSamples)
plt.imshow(datamatrixFloat, interpolation='nearest', cmap=plt.cm.ocean, aspect='auto', origin ='bottom',extent=[0, noOfSamples, 0, noOfRecs])
plt.xlabel('t[ns]')
plt.ylabel('No. Of Turn')
#plotting points
#yCnt=0
#for turn in pointsmatrix:
#    #print(turn)
#    for bunch in turn:
#        for point in bunch:
#            #print(point)
#            plt.plot(point,yCnt, 'ro')
#    yCnt+=1
plt.show()


'''
Plot one iteration
'''

#original data
x = np.arange(noOfSamples)
gx = datamatrix[1700]
gxP = (datamatrix[1700]/50) + 500


levelOfintersectionUp = 150# int(minV + 0.25*ptp)
levelOfintersectionDown =-150 # int(maxV - 0.25*ptp)
fup = np.array([levelOfintersectionUp]*noOfSamples)
fdown = np.array([levelOfintersectionDown]*noOfSamples)

#differential of the signal
tempArrayDer = np.array([0]*noOfSamples)
tempArrayDer[1:] = np.diff(gx)
tempArrayDer[0] = tempArrayDer[1]

#filtering
from scipy import signal
import time
N = 2
fc = 0.1
xf, zf = signal.butter(N,fc,'low')
t1 = time.time()
tempArrayDerF = signal.filtfilt(xf , zf, tempArrayDer, padlen=50)
t2 = time.time()
print(t2-t1)

idxUp = np.argwhere(np.diff(np.sign(levelOfintersectionUp - tempArrayDer))).flatten()
idxDown = np.argwhere(np.diff(np.sign(levelOfintersectionDown - tempArrayDer))).flatten()

plt.figure()
#flattenArray = np.array(listOfBunches).flatten()
#plt.plot(x[flattenArray], gx[flattenArray], 'ro')
plt.plot(x[idxUp], fup[idxUp], 'ro')
plt.plot(x[idxDown], fdown[idxDown], 'ro')
plt.plot(x, gxP, 'r:','-',label='Original signal scalled')
plt.plot(x, tempArrayDer, '-',label='Derivative of original signal')
plt.plot(x, tempArrayDerF, '-',label='Filtered deriv. of original signal')
plt.xlabel('t [ns]')
plt.ylabel('diff(Signal)')
plt.legend()
#
plt.show()



'''
Gradient and 2d plot for filtered version
'''
'''
Gradient plot
'''
tempArray = np.array([0]*noOfSamples)
datamatrix = np.reshape(data,(noOfRecs,noOfSamples))
pointsmatrix = np.array([None]*noOfRecs)
x = np.arange(0, 2240)

datamatrixFloat = np.array(datamatrix, dtype=float)
cnt=0
#initialise array for differentiated signal
tempArrayDer = np.array([0]*noOfSamples)

#initialize filter
N = 2
fc = 0.1
xf, zf = signal.butter(N,fc,'low')
#tempArrayDerF = signal.filtfilt(xf , zf, turn, padlen=50)


def CalcArrOfLenFilt(Arr):
    gx=Arr
    #gx = datamatrix[2700]
    tempArray[1:] = np.diff(gx)
    tempArray[0] = tempArray[1]
    gx=tempArray
    
    
    gx = signal.filtfilt(xf , zf, gx, padlen=50)
    
    minV = np.min(gx)
    maxV = np.max(gx)
    ptp = maxV-minV
    levelOfintersectionUp = 150# int(minV + 0.25*ptp)
    levelOfintersectionDown =-150 # int(maxV - 0.25*ptp)
    
    #gx[(gx > levelOfintersectionUp) & (gx < levelOfintersectionDown)] = 0
    
    thoseUp = (gx < levelOfintersectionUp) 
    thoseDown = (gx > levelOfintersectionDown)
    
    
    listOfBunches=[]
    
    
    #state 0 = zero level, 0/1 - first up, 1/2 second up, 2/3 first down, 3/0 second down, back to 0
    state = 0
    
    first=None
    last=None
    
    #is the bunch to be ignored?
    ignore=False
    #check what is on the beginning
    if(thoseUp[0]==False):
        #this is upper diff
        ignore=True
        state=2
    elif(thoseDown[0]==False):
        #this is lower diff
        ignore=True
        state=3
    
    
    #
    ##optimisation by embedding one if on another
    #Finite state machine
    NoEl=1
    while NoEl < noOfSamples:
        if(state==0):
            if(thoseUp[NoEl]==False):
                #print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                first=NoEl
                #number of samples to ignore
                NoEl=NoEl+15
                state=1
        elif(state==1):
            if(thoseUp[NoEl]==True):
                #print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                #number of samples to ignore
                NoEl=NoEl+15
                state=2
        elif(state==2):
            if(thoseDown[NoEl]==False):
                #number of samples to ignore
                #print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                NoEl=NoEl+15
                state=3
        elif(state==3):
            if(thoseDown[NoEl]==True):
                #number of samples to ignore
                #print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                last=NoEl
                if(ignore==False):
                    #add to list
                    listOfBunches.append([first,last]) 
                else:
                    ignore=False
                state=0
                NoEl=NoEl+15
        #always increment by one        
        NoEl=NoEl+1
        
    return listOfBunches

for turn in datamatrix:
    #diff operation
    
    pointsmatrix[cnt]=np.array(CalcArrOfLenFilt(turn), dtype=int)
    recordPoints = np.array(pointsmatrix[cnt])
    datamatrixFloat[cnt,recordPoints]=np.nan
    cnt+=1

plt.figure()
current_cmap = matplotlib.cm.get_cmap()
current_cmap.set_bad(color='red')
x = np.arange(noOfSamples)
plt.imshow(datamatrixFloat, interpolation='nearest', cmap=plt.cm.ocean, aspect='auto', origin ='bottom',extent=[0, noOfSamples, 0, noOfRecs])
plt.xlabel('t[ns]')
plt.ylabel('No. Of Turn')
plt.show()
'''
One turn/2d plot
'''

#original data
x = np.arange(noOfSamples)
gx = datamatrix[1700]
gxP = (datamatrix[1700]/50) + 500


levelOfintersectionUp = 150# int(minV + 0.25*ptp)
levelOfintersectionDown =-150 # int(maxV - 0.25*ptp)
fup = np.array([levelOfintersectionUp]*noOfSamples)
fdown = np.array([levelOfintersectionDown]*noOfSamples)

#differential of the signal
tempArrayDer = np.array([0]*noOfSamples)
tempArrayDer[1:] = np.diff(gx)
tempArrayDer[0] = tempArrayDer[1]

#filtering
from scipy import signal
import time
N = 2
fc = 0.1
xf, zf = signal.butter(N,fc,'low')
t1 = time.time()
tempArrayDerF = signal.filtfilt(xf , zf, tempArrayDer, padlen=50)
t2 = time.time()
print(t2-t1)

idxUp = np.argwhere(np.diff(np.sign(levelOfintersectionUp - tempArrayDerF))).flatten()
idxDown = np.argwhere(np.diff(np.sign(levelOfintersectionDown - tempArrayDerF))).flatten()

plt.figure()
#flattenArray = np.array(listOfBunches).flatten()
#plt.plot(x[flattenArray], gx[flattenArray], 'ro')
plt.plot(x[idxUp], fup[idxUp], 'ro')
plt.plot(x[idxDown], fdown[idxDown], 'ro')
plt.plot(x, gxP, 'r:','-',label='Original signal')
plt.plot(x, tempArrayDer, '-',label='Derivative of original signal')
plt.plot(x, tempArrayDerF, '-',label='Filtered deriv. of original signal')
plt.xlabel('t [ns]')
plt.ylabel('diff(Signal)')
#
plt.legend()
plt.show()
