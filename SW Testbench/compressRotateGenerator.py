# -*- coding: utf-8 -*-
"""
Compress a frame.
arraySamples - input signal
"""

import splitgenerator as sg
import scipy.integrate as integrate
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
from scipy.optimize import fsolve

'''
Function of performing bunch Rotation on two bunches after split.
'''
def bunchRotation(arraySamples, mi, tau, divisionFactorOfTau, listOfZeros, listOfPositions,bunchLen, bunchFromTo):
    zeroPoint=0
    #Generate time axis.
    t = np.linspace(bunchFromTo[0], bunchFromTo[1],  bunchLen, endpoint=False)
    
    #Coordinates of zeros
    zeroA1=listOfZeros[0]
    zeroA2=listOfZeros[1]
    zeroB1=listOfZeros[2]
    zeroB2=listOfZeros[3]
    
    #Positions of two bunches
    positionA = listOfPositions[0]
    positionB = listOfPositions[1]
    
    targetTau=tau/divisionFactorOfTau

    if(targetTau>=tau):
        print("Please set a lower target tau than initial tau")
    else:
        #Calculate integral of initial profiles
        areaInitialBunch1,er=integrate.quad(lambda t: sg.DistrVect(t,positionA,tau,mi,zeroA1,zeroA2), zeroA1, zeroA2)
        areaInitialBunch2,er=integrate.quad(lambda t: sg.DistrVect(t,positionB,tau,mi,zeroB1,zeroB2), zeroB1, zeroB2)
#        print(areaInitialBunch1)
#        print(areaInitialBunch2)

        '''
        Generate profile for tau equal the targetTau. Then slowly increase amplitude A until the integral of new profile is larger
        than the integral of initial profile.
        '''

        newzeroA1=zeroA1 + 0.5*tau/divisionFactorOfTau
        newzeroA2=zeroA2 - 0.5*tau/divisionFactorOfTau
        newzeroB1=zeroB1 + 0.5*tau/divisionFactorOfTau
        newzeroB2=zeroB2 - 0.5*tau/divisionFactorOfTau
       
        
        increaseFactorOfA=0.002

        found=False
        A1=1.0
        while found==False:
            areaBunch1,er=integrate.quad(lambda t: sg.DistrVect(t,positionA,targetTau,mi,newzeroA1,newzeroA2,A1), newzeroA1, newzeroA2)
            print(areaInitialBunch1)
            print(areaBunch1)
            if(abs(areaInitialBunch1)<abs(areaBunch1)):
                A1=A1-increaseFactorOfA
                found=True
            else:
                A1=A1+increaseFactorOfA
        
        found=False    
        A2=1.0
        while found==False:    
            areaBunch2,er=integrate.quad(lambda t: sg.DistrVect(t,positionB,targetTau,mi,newzeroB1,newzeroB2,A2), newzeroB1, newzeroB2)
            if(abs(areaInitialBunch2)<abs(areaBunch2)):
                A2=A2-increaseFactorOfA
                found=True
            else:
                A2=A2+increaseFactorOfA
        
#        print(A1)
#        print(A2)
#        
#        print(t,positionA,targetTau,mi,newzeroA1,newzeroA2,A1)
        leftBunch = sg.DistrVectArr(t,positionA,targetTau,mi,newzeroA1,newzeroA2,A1)
        rightBunch = sg.DistrVectArr(t,positionB,targetTau,mi,newzeroB1,newzeroB2,A2) 
        
        outputFrame=np.array([0.0]*bunchLen)
        outputFrame[0:int(bunchLen/2)]=leftBunch[0:int(bunchLen/2)]
        outputFrame[int(bunchLen/2):bunchLen-1]=rightBunch[int(bunchLen/2):bunchLen-1]
        pylab.plot(t,sg.DistrVectArr(t,positionA,tau,mi,zeroA1,zeroA2))
        pylab.plot(t,outputFrame)
        
        return outputFrame,A1,A2,[newzeroA1,newzeroA2,newzeroB1,newzeroB2]
        
        
def batchCompression(arraySamples, mi, tau, shift, listOfZeros, listOfPositions,bunchLen, bunchFromTo, A1, A2):
    zeroPoint=0
    #Generate time axis.
    t = np.linspace(bunchFromTo[0], bunchFromTo[1],  bunchLen, endpoint=False)
    
    #Coordinates of zeros
    zeroA1=listOfZeros[0]
    zeroA2=listOfZeros[1]
    zeroB1=listOfZeros[2]
    zeroB2=listOfZeros[3]
    
    #Positions of two bunches
    positionA = listOfPositions[0] + shift
    positionB = listOfPositions[1] - shift

    #move zeros
    newzeroA1=zeroA1 + shift
    newzeroA2=zeroA2 + shift
    newzeroB1=zeroB1 - shift
    newzeroB2=zeroB2 - shift

    #generate new bunches
    leftBunch = sg.DistrVectArr(t,positionA,tau,mi,newzeroA1,newzeroA2,A1)
    rightBunch = sg.DistrVectArr(t,positionB,tau,mi,newzeroB1,newzeroB2,A2)

    outputFrame=np.array([0.0]*bunchLen)
    outputFrame[0:int(bunchLen/2)]=leftBunch[0:int(bunchLen/2)]
    outputFrame[int(bunchLen/2):bunchLen-1]=rightBunch[int(bunchLen/2):bunchLen-1]
    pylab.plot(t,outputFrame)

    return outputFrame, [newzeroA1,newzeroA2,newzeroB1,newzeroB2], [positionA,positionB]

