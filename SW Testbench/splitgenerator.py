# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 14:27:28 2019

@author: pakozlow
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
from scipy.optimize import fsolve
import scipy.integrate as integrate
#
#def DistrVectArr(t,tx,Tau,mi,fromx,tox,A=1):
#    zeroArr = np.array([0.0]*len(t), dtype=float)
#    result = A*((1 - 4*((t-tx)/Tau)**2)**mi)
#    zeroArr[(t>tx-Tau/2)*(t<tx+Tau/2)]=result[(t>tx-Tau/2)*(t<tx+Tau/2)]
#    return zeroArr

def DistrVectArr(t,tx,Tau,mi,fromx,tox,A=1):
    zeroArr = np.array([0.0]*len(t), dtype=float)
    result = A*((1 - 4*((t-tx)/Tau)**2)**mi)
    zeroArr[(t>fromx)*(t<tox)]=result[(t>fromx)*(t<tox)]
    return zeroArr

def DistrVect(t,tx,Tau,mi,fromx,tox,A=1):
    if((t>fromx) or (t<tox)):
        result = A*((1 - 4*((t-tx)/Tau)**2)**mi)
    else:
        result=0
    return result   
    
##    return (1 - 4*((t-tx)/Tau)**2)**mi
#    def myfunc(t, tx,Tau,mi):
#        print(t,'',fromx,'',tox)
#        if t>=fromx and t<=tox:
#            return (1 - 4*((t-tx)/Tau)**2)**mi
#            print(t,'',fromx,'',tox)
#        else:
#            return 0
#
#    vfunc = np.vectorize(myfunc)
#    return vfunc(t,tx,Tau,mi)

def gaussianDistr(t,tx,Tau,mi):
    return (1 - 4*((t-tx)/Tau)**2)**mi
#    def myfunc(t, tx,Tau,mi):
#        if t>=-2.0 and t<=2.0:
#            return (1 - 4*((t-tx)/Tau)**2)**mi
#        else:
#            return 0
#
#    vfunc = np.vectorize(myfunc)
#    return vfunc(t,tx,Tau,mi)
    

#finding intersection of two gaussian functions
def findIntersection(gaussianDistr,x0,t0,t1,Tau,mi):
    return fsolve(lambda t : gaussianDistr(t,t0,Tau,mi) - gaussianDistr(t,t1,Tau,mi),x0)


'''
Split based on numpy arrays
'''
def testSplit():
    #1. generate gaussian shape 
    bunchLen=1000
    halfBunchLen=bunchLen/2
    
    Tau=2
    mi=2
    
    #set bunches
    t1s=0.0
    t1 = np.linspace(-1, 1,  bunchLen, endpoint=False)
    e1 = (1 - 4*((t1)/Tau)**2)**mi
    t1=t1*halfBunchLen + halfBunchLen
    
    
    t2s=100
    t2 = np.linspace(-1, 1,  bunchLen, endpoint=False)
    e2 = (1 - 4*((t2)/Tau)**2)**mi
    #shift
    t2 = t2*halfBunchLen + halfBunchLen + t2s
    
    #plot1 = plt.plot(t1,e1, '-')
    #
    #plot2 = plt.plot(t2,e2, '-')
    #plt.show()
    
    
    #
    lenOfCommonPart=bunchLen-t2s
    xCommon = np.arange(0, lenOfCommonPart)
    if(lenOfCommonPart>0):
        commonPart1=np.array([0]*lenOfCommonPart, dtype=float)
        commonPart2=np.array([0]*lenOfCommonPart, dtype=float)
    
        for x in range(0, lenOfCommonPart):
            ind=x+t2s
            commonPart1[x]=e1[ind]
            commonPart2[x]=e2[x]
        
        #find center
        idx = np.argwhere(np.abs(np.diff(commonPart1 - commonPart2)) == np.min(np.abs(np.diff(commonPart1 - commonPart2))))
        #idx = np.argwhere(np.diff(commonPart1 - commonPart2)).flatten()
        points = plt.plot(xCommon[idx], commonPart2[idx], 'ro')
        plot1 = plt.plot(commonPart1, '-')
        plot2 = plt.plot(commonPart2, '-')
        plt.show()





'''
Split based on formula
'''
def testSplitFormula():
    #1. define distribution
    
    bunchLen=100
    t = np.linspace(-2, 2,  bunchLen, endpoint=False)
    
    
    t0=0
    t1=0.5 #it is increasing
    ## gaussian signal from binomial form
    #bunchLen=len(t)*4
    #plt.figure()
    Tau=2
    mi=3
    #e0 = (1 - 4*((t+t0)/Tau)**2)**mi
    #e1 = (1 - 4*((t+t1)/Tau)**2)**mi
    
    #2. Find intersection point and calculate integral (or rather discrete version - a sum)
    
    
    
    result = findIntersection(gaussianDistr,0.0,t0,t1,Tau,mi)
    
    
    #plot the gaussian distributions
    pylab.plot(t,gaussianDistr(t,t0,Tau,mi),t,gaussianDistr(t,t1,Tau,mi),result,gaussianDistr(result,t0,Tau,mi),'ro')
    #plot the overlapping areas to integrate
    ovlap1 = plt.fill_between(t[t>result], 0, gaussianDistr(t[t>result],t0,Tau,mi),alpha=0.3)
    ovlap2 = plt.fill_between(t[t<result], 0, gaussianDistr(t[t<result],t1,Tau,mi),alpha=0.3)
    
    #verifying zeros 
    zeros1=fsolve(gaussianDistr,result,args=(t0,Tau,mi))
    zeros2=fsolve(gaussianDistr,result,args=(t1,Tau,mi))
    
    pylab.plot(zeros1,gaussianDistr(zeros1,t0,Tau,mi),'ro',zeros2,gaussianDistr(zeros2,t1,Tau,mi),'ro')
    
    
    area1,er=integrate.quad(lambda t: gaussianDistr(t,t0,Tau,mi), result, zeros1)
    area2,er=integrate.quad(lambda t: gaussianDistr(t,t1,Tau,mi), zeros2, result)
    area = area1 + area2
    print("Common area under curves ", area)
    
    #validity check
    areafull,er=integrate.quad(lambda t: gaussianDistr(t,t0,Tau,mi), -1, 1)
    if(areafull!=1):
        print("Area of one initial bunch is", areafull)
    
        
    pylab.show()

'''
finding few iterations of split/merge
'''

def generateSplitRecords(NoOfShifts=30,shiftSize=0.05, mi=2, Tau_Init=2, bunchLen=1000, bunchFromTo = [-10,10]):
    # Global constants
    mi=mi
    bunchLen=bunchLen
    bunchFromTo = bunchFromTo
    t = np.linspace(bunchFromTo[0], bunchFromTo[1],  bunchLen, endpoint=False)
    #amplitude = 1
    
    #size of shift
    shiftSize = shiftSize
    
    
    # 0. Generate initial gaussian shape
    t0_Init=0
    Tau_Init=Tau_Init
    e_Init = (1 - 4*((t+t0_Init)/Tau_Init)**2)**mi
    # Calculate the overal distribution of particles
    areafull,er=integrate.quad(lambda t: gaussianDistr(t,t0_Init,Tau_Init,mi), -1, 1)
    
    #pylab.plot(t,e_Init)
    
    #1. Try to split into two different distributions with the same integral
    #1.1 Increase distance
    
    #To obtain 10 records of data during split
    #the number of frames/y-resolution
    NoOfShifts=NoOfShifts
    
    arrayOfTau=[]
    arrayOfDiffInteg=[]
    
    #generate array of frames
    arrayOfRecords=np.array([np.array([0.0]*bunchLen)]*NoOfShifts)
    arrayOfRecords[0]=DistrVectArr(t,t0_Init,Tau_Init,mi,-Tau_Init/2,Tau_Init/2)
    
    #initial positions
    t0=0
    t1=0
    
    #list of zeros for each iteration
    listOfZeros=[]
    listOfZeros.append([-Tau_Init/2,Tau_Init/2])
    #list of positions for each iteration
    listOfPositions=[]
    listOfPositions.append([t0])
    
    lastTau=Tau_Init-(0.05*Tau_Init)
    for x in range(1, NoOfShifts):
        t0=t0-shiftSize
        t1=t1+shiftSize
        zeroPoint=0
    
        # decrease the area of bunches until it is lower than initial area
        # theMostSuitedTauForTheIteration=0
    
        found=False
        #loop until a proper tau is found
    
    
        #zero points check
    #        zeros1=fsolve(gaussianDistr,zeroPoint,args=(t0,lastTau,mi))
    #        zeros2=fsolve(gaussianDistr,zeroPoint,args=(t1,lastTau,mi))

    
        #integrate overlapped area
        while found==False:
            zerosA1=-(lastTau/2)+t0
            zerosA2=(lastTau/2)+t0
            zerosB1=-(lastTau/2)+t1
            zerosB2=(lastTau/2)+t1
            
            OvlapArea1,er=integrate.quad(lambda t: DistrVect(t,t0,lastTau,mi,zerosA1,zerosA2), zerosA2, zeroPoint)
            OvlapArea2,er=integrate.quad(lambda t: DistrVect(t,t1,lastTau,mi,zerosB1,zerosB2), zeroPoint, zerosB1)
            overlapArea = abs(OvlapArea1 + OvlapArea2)
            areaOneBunch,er=integrate.quad(lambda t: DistrVect(t,t0,lastTau,mi,zerosA1,zerosA2), zerosA1, zerosA2)
            bimodalDistrArea= 2*areaOneBunch - overlapArea
            if(bimodalDistrArea<areafull):
                found=True
                arrayOfRecords[x][0:int(bunchLen/2)]=DistrVectArr(t,t0,lastTau,mi,zerosA1,zerosA2)[0:int(bunchLen/2)] 
                arrayOfRecords[x][int(bunchLen/2):bunchLen-1]=DistrVectArr(t,t1,lastTau,mi,zerosB1,zerosB2)[int(bunchLen/2):bunchLen-1]
                listOfZeros.append([zerosA1,zerosA2])
                listOfZeros.append([zerosB1,zerosB2])
                listOfPositions.append([t0])
                listOfPositions.append([t1])
            else:
                print('not found')
                lastTau=lastTau-(0.05*lastTau)
    

    
#    plt.figure()
#    for x in range(0, NoOfShifts):
#        pylab.plot(t,arrayOfRecords[x])
#    pylab.show()
#    plt.figure()
#    plt.imshow(arrayOfRecords, cmap="hot")
#    pylab.show()
    
    return arrayOfRecords, lastTau, listOfPositions, listOfZeros
