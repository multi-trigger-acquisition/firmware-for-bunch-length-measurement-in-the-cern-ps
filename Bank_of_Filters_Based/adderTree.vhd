library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.sort_pkg.all;
use IEEE.math_real."log2";
use IEEE.MATH_REAL;

entity adderTree is
	generic (

		--number of elements to convolve (if the number will change, the coefficients have to be regenerated)
		noOfInputs 		: natural ;
		-- the code is synthesisable, it is only parameter which is calculated
		height 			: natural ;
		--delay to compensate the difference between filters
		maxCompensateDelay 	: natural
	);
	port (		
			clk		: in std_logic;
			reset		: in std_logic;
			inputFromMult	: in ArrSig_t(0 to (2**height)-1);
			-- TO DO
			-- to prevent overflow the height of tree has to be added to 31(every addition increase max by one bit)
			output	: out std_logic_vector(31 downto 0)
	);
end entity;

architecture sequential of adderTree is

--delay array 2**(N-2) - 2**(n-2) - N = max scale n-scale of the iteration
signal shiftSignalArray 	: ArrSig_t(0 to 2*(1+maxCompensateDelay-(2**(height-1)+1)/2)+1);

--array of connections in the adder tree
signal ArrSig_buf 		: ArrSig_t(0 to (2**(height+2))-1);


--intermediate output
signal intermOutput	:  std_logic_vector(31 downto 0);

begin

--adding delay to fit the the time-line of each filter to the one with longest delay
Registers: process (clk,reset)
begin
	if Falling_edge(clk) then
	if reset = '1' then
		shiftSignalArray <= ((others=> (others=>'0')));
	else
 		shiftSignalArray(0 to 2*(1+maxCompensateDelay-(2**(height-1)+1)/2))<= intermOutput & shiftSignalArray(0 to 2*(maxCompensateDelay-(2**(height-1)+1)/2)+1);
	end if;
	end if;
end process;

--Registers: process (clk,reset)
--begin
--	if reset = '1' then
--		shiftSignalArray <= ((others=> (others=>'0')));
--	elsif Falling_edge(clk) then
----		report "maxCompensateDelay: " 	& INTEGER'image(maxCompensateDelay);
----		report "thatTelay: " 	& INTEGER'image(2*(1+maxCompensateDelay-(2**(height-1)+1)/2));
--		shiftSignalArray(0 to 2*(1+maxCompensateDelay-(2**(height-1)+1)/2))<= intermOutput & shiftSignalArray(0 to 2*(maxCompensateDelay-(2**(height-1)+1)/2)+1);
--	end if;
--end process;


--
--
--output <= shiftSignalArray(maxCompensateDelay-((2**(height-1)+1)/2)+1);
output <= shiftSignalArray(2*(1+maxCompensateDelay-(2**(height-1)+1)/2));


-- A simple adder tree, analogous situation like in the MaxMinFind module
process(clk,reset)
variable heightMod : natural := height-1;
  begin
	if Falling_edge(clk) then 
	if reset = '1' then
		--initial values ofor the array of connections
		for k in 0 to (2**(heightMod+2))-1 loop
			ArrSig_buf(k)<= std_logic_vector(to_signed(0, 32));
		end loop;
		intermOutput<=ArrSig_buf(0);
	else 
		for k in 0 to heightMod loop
			for el in 0 to 2**k-1 loop
				--loop through each adder
				ArrSig_buf(2**k + el - 1) <= std_logic_vector(
				signed(ArrSig_buf(2**(k+1)-1 + 2 * el)) + 
				signed(ArrSig_buf(2**(k+1) + 2 * el))
				);
			end loop;
		end loop;
		intermOutput<=ArrSig_buf(0);
		ArrSig_buf(((2**(heightMod+2))-1 ) - 2*(2**heightMod) to (2**(heightMod+2))-2) <= inputFromMult;
    	end if;
	end if;
end process;


--process(clk,reset)
--variable heightMod : natural := height-1;
--  begin
--	if reset = '1' then
--		--initial values ofor the array of connections
--		for k in 0 to (2**(heightMod+2))-1 loop
--			ArrSig_buf(k)<= std_logic_vector(to_signed(0, 32));
--		end loop;
--		intermOutput<=ArrSig_buf(0);
--	elsif Falling_edge(clk) then 
--		for k in 0 to heightMod loop
--			for el in 0 to 2**k-1 loop
--				--loop through each adder
--				ArrSig_buf(2**k + el - 1) <= std_logic_vector(
--				signed(ArrSig_buf(2**(k+1)-1 + 2 * el)) + 
--				signed(ArrSig_buf(2**(k+1) + 2 * el))
--				);
--			end loop;
--		end loop;
--		intermOutput<=ArrSig_buf(0);
--		ArrSig_buf(((2**(heightMod+2))-1 ) - 2*(2**heightMod) to (2**(heightMod+2))-2) <= inputFromMult;
--    	end if;
--end process;
--

end architecture;
