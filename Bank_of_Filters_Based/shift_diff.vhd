--  Solution to Counter Exercise

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_std.all;

entity Shift_diff is
  port (Clock, Reset, Enable: in Std_logic;
	-- 1 - the data output is a derivative, therefore baseline even though is not perferct 
	--the influence of the imprefection is minimized
	ValidOut: out Std_logic;
        DataIn: in Std_logic_vector(15 downto 0);
        DataOut:   out Std_logic_vector(16 downto 0));
end;



architecture RTL of Shift_diff is
  signal DiffResultExt: signed(16 downto 0);
  signal A1: unsigned(15 downto 0);
  signal A2: unsigned(15 downto 0);
begin

--signal converted to unsigned vector
DataOut <= Std_logic_vector(unsigned(DiffResultExt));

--it will require overflow extension, that's why it has one more bit
DiffResultExt<=signed(Resize(unsigned(A1),17)) - signed(Resize(unsigned(A2), 17));
  process (Clock, Reset)
  begin
    
    if Reset = '1' then
      A1 <= to_unsigned(3000, A1'length);
      A2 <= to_unsigned(3000, A1'length);
    elsif Rising_edge(Clock) then
      if Enable = '1' then
	A1<=unsigned(DataIn); 
	A2<=A1;
	ValidOut<='1';
      else
	ValidOut<='0';
      end if;
    end if;


end process;

end;