library IEEE;
use IEEE.Std_logic_1164.all;library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.Numeric_std.all;
use STD.TEXTIO.all;
use IEEE.Std_logic_textio.all;
use work.SpyOnMySigPkg.all ;

architecture TB1 OF BunchLenMeasTB is
  signal Clock, Reset, Enable,OutValid,startFineMeas     	: STD_LOGIC;
  signal DataIn							: STD_LOGIC_VECTOR (15 downto 0);
  signal Expected_DataOut,DataOut,MaxValueOut			: STD_LOGIC_VECTOR (15 downto 0);
  signal OK : Boolean := True;
  signal globalCounter							: signed(32 downto 0);

begin

  uut: entity work.topDerivThres
  port map (
    Clock 	 => Clock,
    Reset        => Reset,
    Enable       => Enable,
    DataIn       => DataIn,
    DataOut	 => DataOut,
    OutValid 	 => OutValid,
    MaxValueOut  => MaxValueOut,
    startFineMeas => startFineMeas
  );


--clock
 Clk: process
  begin
    while now <= 350000 NS loop
      Clock <= '0';
      wait for 5 NS;
      Clock <= '1';
      wait for 5 NS;
    end loop;
    wait;
  end process;

GlobCount: process(Clock,Reset)
begin
if rising_edge(Clock) then
	if(Reset='1') then
		globalCounter <= to_signed(0,globalCounter'length);
	else
		globalCounter <= globalCounter + 1;
	end if;
end if;
end process;


  ReadFile : process
	file F						: TEXT open READ_MODE is "signalForTB.txt";
	variable L					: LINE;
--	variable VClock, VReset, VEnable, VOutValid	: STD_LOGIC;
	variable VDataIn 				: Integer;
	variable VDataOutExpected 			: STD_LOGIC_VECTOR (15 downto 0);
	variable NoOfBunches				: Integer := 0;
	variable LenOfBunches				: Integer := 0;
	variable First					: BOOLEAN := TRUE;
	variable StartMeasLen				: BOOLEAN := TRUE;
	variable NoOfSample				: Integer := 0;
  begin
	if First then
      		-- read expected values
      		Readline(F, L);
		--load expected result
		READ(L, NoOfBunches);
		READ(L, LenOfBunches);
		report "No of bunches: " 	& INTEGER'IMAGE(NoOfBunches);
		report "Length of bunches: " 	& INTEGER'IMAGE(LenOfBunches);
      		First := FALSE;
		--initialize DUT
		wait until Falling_edge(Clock);
		Reset<='1';
		Enable<='0';
		DataIn<=STD_LOGIC_VECTOR(to_signed(0,DataIn'length));
		wait until Falling_edge(Clock);
		Reset<='0';
		Enable<='0';
		DataIn<=STD_LOGIC_VECTOR(to_signed(0,DataIn'length));
    	end if;

	wait until Falling_edge(Clock);
    	Readline(F, L);
    	Read(L, VDataIn);
	--report "NExt cycle";
	--report "Data in: " 	& INTEGER'IMAGE(VDataIn);
    	Reset  <= '0';
    	Enable <= '1';
	DataIn <=std_logic_vector(to_signed(VDataIn,DataIn'length));

	-- gathering information about start and end time of a bunch
	if GlobalState = FIRST_UP and StartMeasLen = FALSE then 
		StartMeasLen:= TRUE;
		report "From: "  & INTEGER'image(to_integer(globalCounter) mod 1000);
	end if;
	if GlobalState = SECOND_DOWN and StartMeasLen = TRUE then 
		StartMeasLen:= FALSE;
		report "To: " 	& INTEGER'image(to_integer(globalCounter) mod 1000);
	end if;

	--length of the bunch
	if unsigned(DataOut) > 0 then 
		report "Length: " 	& INTEGER'image(to_integer(unsigned(DataOut)));
	end if;


--code here

    --wait;
  end process;




end TB1;




--architecture TB1_raw OF BunchLenMeasTB is
--  signal Clock, Reset, Enable,OutValid     	: STD_LOGIC;
--  signal DataIn					: STD_LOGIC_VECTOR (15 downto 0);
--  signal Expected_DataOut,DataOut		: STD_LOGIC_VECTOR (15 downto 0);	
--  signal OK : Boolean := True;
--
--begin
--
--  uut: entity work.topDerivThres
--  port map (
--    Clock 	 => Clock,
--    Reset        => Reset,
--    Enable       => Enable,
--    DataIn       => DataIn,
--    DataOut	 => DataOut,
--    OutValid 	 => OutValid
--  );
--
--
----clock
-- Clk: process
--  begin
--    while now <= 300 NS loop
--      Clock <= '0';
--      wait for 5 NS;
--      Clock <= '1';
--      wait for 5 NS;
--    end loop;
--    wait;
--  end process;
--
--  process
--  begin
--
--    Reset<='0';Enable<='0';DataIn<=STD_LOGIC_VECTOR(to_signed(0,DataIn'length));   Expected_DataOut<=(others => 'X')
--;wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    --reset and initialise registers
--    Reset<='1';Enable<='0';DataIn<=STD_LOGIC_VECTOR(to_signed(0,DataIn'length));   Expected_DataOut<=(others => '0');
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    Reset<='0';Enable<='0';DataIn<=(others => '-');				     Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(0,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    Reset<='0';Enable<='0';DataIn<=STD_LOGIC_VECTOR(to_signed(3400,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(0,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(3500,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(500,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(3700,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(200,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(3900,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(200,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(4100,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(200,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(3900,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(-200,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(3800,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(-100,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    Reset<='0';Enable<='0';DataIn<=STD_LOGIC_VECTOR(to_signed(3700,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(-100,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--
--Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(6900,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(3100,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--
--Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(10000,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(3100,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--
--Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(6900,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(-3100,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--
--Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(6700,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(-200,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--
--    wait;
--  end process;
--
--
--end TB1_raw;