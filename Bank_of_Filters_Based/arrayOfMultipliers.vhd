library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.sort_pkg.all;
use IEEE.math_real."log2";
use IEEE.MATH_REAL;

entity arrayOfMultipliers is
	generic (

		--number of elements to convolve (if the number will change, the coefficients have to be regenerated)
		noOfInputs 		: natural ;
		-- the code is synthesisable, it is only parameter which is calculated
		height 			: natural 
	);
	port (
			clk		: in std_logic;
			reset		: in std_logic;
			inputShiftArr	: in InputArrSig_t(0 to (2**height)-1);
			inputCoeff	: in InputArrSig_t(0 to (2**height)-1);
			outputToAdd	: out ArrSig_t(0 to (2**height)-1)
	);
end entity;


architecture sequential of arrayOfMultipliers  is
begin





Registers: process (clk,reset)
begin
	if Falling_edge(clk) then
		if reset = '1' then
			outputToAdd <= ((others=> (others=>'0')));
		else
			for tapEl in 0 to noOfInputs-1 loop
				--loop through each tap
				outputToAdd(tapEl) <= std_logic_vector(signed(inputShiftArr(tapEl)) * signed(inputCoeff(tapEl)));
			end loop;
		end if;
	end if;
end process;

end architecture;
