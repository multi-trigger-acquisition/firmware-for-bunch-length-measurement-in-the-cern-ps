library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_std.all;

--scope to internal signals
package SpyOnMySigPkg is
  -- synthesis translate_off   
	type StateType is (ZERO, FIRST_UP, FIRST_DOWN, SECOND_DOWN,REARM);
	signal GlobalState: StateType;
  -- synthesis translate_on 
end package SpyOnMySigPkg ;

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_std.all;
use work.SpyOnMySigPkg.all ;

-- architecture for catching thershold that is based on derivative
-- Template for Finite State Machine Exercise
entity FSM_ThresDetec is
  port (Clock, Reset, InValid : in STD_LOGIC;
	DataIn: in Std_logic_vector(16 downto 0);
	OutValid, ResetMaxValue: out STD_LOGIC;
	DataOut: out Std_logic_vector(15 downto 0);
	timestampMax : out Std_logic_vector(15 downto 0)
	);
end;

architecture RTL_deriv of FSM_ThresDetec is

--type StateType is (ZERO, FIRST_UP, FIRST_DOWN, SECOND_DOWN,REARM);
signal State, NextState: StateType;

signal startCounterOmit: STD_LOGIC;
signal startCounterLength: STD_LOGIC;
-- start bunch TS-------- noise omit TS -------- length ->
--signal noiseOmitTS: unsigned(10 downto 0);
--signal lengthBunch: unsigned(10 downto 0);
signal tempOmit: unsigned(10 downto 0);
signal tempLength: unsigned(15 downto 0);

begin



timestampMax<=std_logic_vector(tempLength);

--reset
Registers: process (Clock,Reset)
begin
	if Reset = '1' then
		State<=ZERO;
	elsif Falling_edge(Clock) then
		State <= NextState;
	end if;
end process;

-- scope for testbench purpose
GlobalState<=State;


--it is necessary to put driving signals into sensitivity list
--this is because if we stay in the same state it has to
-- react on driving signals
C_Logic: process (State,inValid,tempOmit,tempLength,DataIn)
begin
--default values
DataOut<=(others => '0');
OutValid<='0';
startCounterOmit<='0';
startCounterLength<='0';
ResetMaxValue<='0';
case State is
when ZERO =>
	--automatically Buttons(0) as dont care?
	if signed(DataIn) > 650 and InValid='1' then
		NextState <= FIRST_UP;
		--run counter to measure the length of the signal
		startCounterLength<='1';
	else 
		NextState <= ZERO;
	end if;
when FIRST_UP =>
	if signed(DataIn) < -600 and InValid='1' then
		NextState <= FIRST_DOWN;
		--run counter and stop next state for some time
		startCounterOmit<='1';
	else
		NextState <= FIRST_UP;
	end if;
when FIRST_DOWN =>
	--temp is the value of counter to wait until noise will not be a problem
	if signed(DataIn) > -600 and InValid='1' and tempOmit>2 then
		NextState <= SECOND_DOWN;
	else
		NextState <= FIRST_DOWN;
	end if;
when SECOND_DOWN =>
	NextState	<= REARM;
	OutValid	<= '1';
	--data out is simply aproximate length
	DataOut		<= std_logic_vector(tempLength);
when REARM =>
	NextState	<= ZERO;
	ResetMaxValue	<= '1';
when others =>
	null;
end case;
end process;




-- counter for measuring bunch length
process(Clock,Reset,startCounterLength)
   begin
	if(Falling_edge(Clock)) then
		if Reset='1'or startCounterLength='1' then
         		tempLength <= (others => '0');
		elsif(InValid='1') then
			tempLength <= tempLength + 1; 
		else
			null;
		end if;

	end if;
end process;


-- counter for ignoring some samples - because of noise
process(Clock,Reset)
   begin
	if(Falling_edge(Clock)) then
		if Reset='1'or startCounterOmit='1' then
         		tempOmit <= (others => '0');
		elsif InValid='1' then
			tempOmit <= tempOmit + 1;  
		else
			null;
		end if;
	end if;
end process;



---- counter for measuring bunch length
--process(Clock,Reset,startCounterLength)
--   begin
--	if Reset='1'or startCounterLength='1' then
--         tempLength <= (others => '0');
--      elsif(Falling_edge(Clock) and InValid='1') then
--	 tempLength <= tempLength + 1;   
--      end if;
--end process;
--
--
---- counter for ignoring some samples - because of noise
--process(Clock,Reset)
--   begin
--      if Reset='1'or startCounterOmit='1' then
--         tempOmit <= (others => '0');
--      elsif(Falling_edge(Clock) and InValid='1') then
--	 tempOmit <= tempOmit + 1;   
--      end if;
--end process;

end;

