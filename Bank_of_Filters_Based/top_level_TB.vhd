library IEEE;
use IEEE.Std_logic_1164.all;library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.Numeric_std.all;
use STD.TEXTIO.all;
use IEEE.Std_logic_textio.all;
use work.sort_pkg.all;
use work.SpyOnMySigPkgMaxMinFind.all ;

entity TopLvLTB is
end;
--declaration of entity--



architecture TB1 OF TopLvLTB is
  signal Clock, Reset, Enable     	: STD_LOGIC;
  signal DataIn							: STD_LOGIC_VECTOR (15 downto 0);
  signal Expected_DataOut,DataOut,MaxValueOut			: STD_LOGIC_VECTOR (15 downto 0);	
  signal OK 							: Boolean := True;

  signal apprxLen 						:	 SLV_16;
  signal apprxLenValid						:	 std_logic;
  signal maxVal							:	 SLV_16;
  signal accurLen						:	 SLV_16;
  signal accurLenValid						:	 std_logic;

  signal globalCounter							: signed(32 downto 0);
begin

  uut: entity work.topDesign
  port map (
    	Clock 	 	=> Clock,
    	Reset        	=> Reset,
    	Enable       	=> Enable,
    	DataIn      	=> DataIn,
	apprxLen	=> apprxLen,
	apprxLenValid	=> apprxLenValid,
	maxVal		=> maxVal,
	accurLen	=> accurLen,
	accurLenValid	=> accurLenValid
  );


--clock
 Clk: process
  begin
    while now <= 1000000 NS loop
      Clock <= '0';
      wait for 5 NS;
      Clock <= '1';
      wait for 5 NS;
    end loop;
    wait;
  end process;

--global counter of the system
GlobCount: process(Clock,Reset)
begin
if rising_edge(Clock) then
	if(Reset='1') then
		globalCounter <= to_signed(0,globalCounter'length);
	else
		globalCounter <= globalCounter + 1;
	end if;
end if;
end process;


  ReadFile : process
	file F						: TEXT open READ_MODE is "signalForTB.txt";
	variable L					: LINE;
--	variable VClock, VReset, VEnable, VOutValid	: STD_LOGIC;
	variable VDataIn 				: Integer;
	variable VDataOutExpected 			: STD_LOGIC_VECTOR (15 downto 0);
	variable NoOfBunches				: Integer := 0;
	variable LenOfBunches				: Integer := 0;
	variable First					: BOOLEAN := TRUE;
	variable StartMeasLen				: BOOLEAN := TRUE;
  begin
	if First then
      		-- read expected values
      		Readline(F, L);
		--load expected result
		READ(L, NoOfBunches);
		READ(L, LenOfBunches);
--		report "No of bunches: " 	& INTEGER'IMAGE(NoOfBunches);
--		report "Length of bunches: " 	& INTEGER'IMAGE(LenOfBunches);
      		First := FALSE;
		--initialize DUT
		--wait until Falling_edge(Clock);
		Reset<='1';
		Enable<='0';
		DataIn<=STD_LOGIC_VECTOR(to_signed(0,DataIn'length));
		wait until Falling_edge(Clock);
		Reset<='0';
		Enable<='0';
		DataIn<=STD_LOGIC_VECTOR(to_signed(0,DataIn'length));
    	end if;

	wait until Falling_edge(Clock);
    	Readline(F, L);
    	Read(L, VDataIn);
--	report "NExt cycle";
--	report "Data in: " 	& INTEGER'IMAGE(VDataIn);
		
    	Reset  <= '0';
    	Enable <= '1';
	DataIn <=std_logic_vector(to_signed(VDataIn,DataIn'length));


	-- gathering information about start and end time of a bunch
--	if GlobalState = FIRST_UP and StartMeasLen = FALSE then 
--		StartMeasLen:= TRUE;
--		report "From: "  & INTEGER'image(to_integer(globalCounter) mod 1000);
--	end if;
--	if GlobalState = SECOND_DOWN and StartMeasLen = TRUE then 
--		StartMeasLen:= FALSE;
--		report "To: " 	& INTEGER'image(to_integer(globalCounter) mod 1000);
--	end if;
--	if validInGlobal='1' then 
--		report "EndingOfAWindow: " 	& INTEGER'image(to_integer(globalCounter) mod 1000);
--	end if;


	--length of the bunch
	if accurLenValid = '1' then 
		report "Length: " 	& INTEGER'image(to_integer(unsigned(accurLen)));
	end if;


    --wait;
  end process;




end TB1;


