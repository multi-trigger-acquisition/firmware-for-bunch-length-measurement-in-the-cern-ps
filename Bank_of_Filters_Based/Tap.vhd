library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.sort_pkg.all;

--	subtype SLV_8  is STD_LOGIC_VECTOR(7 downto 0);        -- define a Byte
--	subtype SLV_16  is STD_LOGIC_VECTOR(15 downto 0);        -- define a Byte
--	type    InputArrInd_t is array(NATURAL range <>) of SLV_8;  -- define a new unconstrained vector of Bytes.
--        type    InputArrSig_t is array(NATURAL range <>) of SLV_16;

entity Tap is
	port (		
			Clock: in std_logic;
			signalIn: in SLV_16;
			coefficient: in SLV_16;
			signalOut: out SLV_16;
			multResult: out STD_LOGIC_VECTOR(31 downto 0)
	);
end entity;
architecture sequential of Tap  is
begin

process (Clock)
  begin
    if Rising_edge(Clock) then
      multResult<=std_logic_vector(signed(signalIn ) * signed(coefficient));
      signalOut	<=signalIn;

    end if;
end process;

end architecture;
