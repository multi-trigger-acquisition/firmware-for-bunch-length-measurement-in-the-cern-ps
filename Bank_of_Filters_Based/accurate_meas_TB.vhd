library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.sort_pkg.all;

ENTITY accurate_meas_TB IS
END ENTITY accurate_meas_TB;

ARCHITECTURE testbench_0 OF accurate_meas_TB IS

constant HEIGHT 				: integer := 7;
signal Clock ,Reset,start  			: STD_LOGIC;
signal OK 					: Boolean := True;
signal SignalIn					: SLV_16;
signal accurateLength				: SLV_16;
signal validLength				: STD_LOGIC;

BEGIN

DUT: entity  work.topAccMeas(TOP_RTL)
	GENERIC MAP(
		sizeOfWindowTop   => 128,
		--should be twice as much as size of window
		sizeOfFrameTop	  => 256,
		thresholdTop	  => 100)
      	PORT MAP(   Clock	 	=>  	Clock,
		Reset	 	=>  	Reset,	
               	start	   	=> 	start, 	
	       	SignalIn	=>	SignalIn,
	       	accurateLength	=>	accurateLength,	
	       	validLength	=> 	validLength)
;



--clock
 Clk: process
  begin
    while now <= 4000 NS loop
      Clock <= '0';
      wait for 5 NS;
      Clock <= '1';
      wait for 5 NS;
    end loop;
    wait;
  end process;




-- input many samples - 400
-- generate parabolic pulse of length 200
sendSamples : process
variable zeroPeriod		: BOOLEAN := TRUE;
variable signalValue		: integer;
variable timeCenter		: integer := -100;
variable bunchWidth		: integer := 200;
  begin
	for bunchIndx in 0 to 400 loop
		if zeroPeriod then
			start<='0';
			Reset<='0';
			SignalIn<=std_logic_vector(to_unsigned(0,SignalIn'length));
			wait until Falling_edge(Clock);
			Reset<='1';
			wait until Falling_edge(Clock);
			Reset<='0';
			wait until Falling_edge(Clock);
			zeroPeriod := FALSE;
		--SignalIn<=std_logic_vector(to_unsigned(0,SignalIn'length));
		--wait for 4000 ns;
		--timeCenter:=-100;
		end if;

		if timeCenter=99 then
			start<='1';
		else
			start<='0';
		end if;
		if bunchIndx > 101 and bunchIndx < 302 then
			signalValue	:= -(timeCenter)**2 + (bunchWidth/2)**2; --1-4*(1*timeCenter/bunchWidth)**2;
			timeCenter	:= timeCenter+1;
			SignalIn	<=std_logic_vector(to_unsigned(signalValue,SignalIn'length));
			report("At time " & integer'image(timeCenter) & " the value is " &
             		integer'image(signalValue));
			wait until Falling_edge(Clock);
		else
			SignalIn<=std_logic_vector(to_unsigned(0,SignalIn'length));
			wait until Falling_edge(Clock);
		end if;
	end loop;

	
end process;


--send signal to compare

--check output
 



--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    --reset and initialise registers
--    Reset<='1';Enable<='0';DataIn<=STD_LOGIC_VECTOR(to_signed(0,DataIn'length));   Expected_DataOut<=(others => '0');
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    Reset<='0';Enable<='0';DataIn<=(others => '-');				     Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(0,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    Reset<='0';Enable<='0';DataIn<=STD_LOGIC_VECTOR(to_signed(3400,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(0,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--



END ARCHITECTURE testbench_0;


