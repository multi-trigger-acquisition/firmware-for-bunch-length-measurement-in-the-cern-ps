--The module cositsts of a tree of comparators.
--The lowest difference between threshold and signal will indicate the moment of the intersection.


-- VHDL2008
--	subtype SLV_8  is STD_LOGIC_VECTOR(7 downto 0);        -- define a Byte
--	subtype SLV_16  is STD_LOGIC_VECTOR(15 downto 0);        -- define a Byte
--	type    InputArrInd_t is array(NATURAL range <>) of SLV_8;  -- define a new unconstrained vector of Bytes.
--        type    InputArrSig_t is array(NATURAL range <>) of SLV_16;
--##generation of coefficients in python
--import numpy as np
--import matplotlib.pyplot as plt
--def ricker(f, length=128, dt=1):
--    t = np.arange(-length/2, (length-dt)/2, dt)
--    y = (1.0 - 2.0*(np.pi**2)*(f**2)*(t**2)) * np.exp(-(np.pi**2)*(f**2)*(t**2))
--    return t, y
--f = 0.018 # A low wavelength of 25 Hz
--t, w = ricker(f)
--w=np.array(w*100, dtype=int)
--plt.plot(t, w)

--library ieee;
--use ieee.std_logic_1164.all;
--use ieee.numeric_std.all;
--
--package sort_pkg is
--	subtype SLV_8  is STD_LOGIC_VECTOR(7 downto 0);        -- define a Byte
--	subtype SLV_16  is STD_LOGIC_VECTOR(15 downto 0);        -- define a Byte
--	type    InputArrInd_t is array(NATURAL range <>) of SLV_8;  -- define a new unconstrained vector of Bytes.
--    	type    InputArrSig_t is array(NATURAL range <>) of SLV_16;
--    	subtype SLV_32  is STD_LOGIC_VECTOR(31 downto 0);
--    	type ArrSig_t is array(NATURAL range <>) of SLV_32;
--    	type ArrSig_t_t is array(NATURAL range <>) of ArrSig_t;
--		
--end package;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.sort_pkg.all;
use IEEE.math_real."log2";
use IEEE.MATH_REAL;

entity waveletTransform is
	generic (
		--number of elements to convolve (if the number will change, the coefficients have to be regenerated)
		lengthOfIns 	: natural  	:= 256 ;
		-- the code is synthesisable, it is only parameter which is calculated
		--height in this case is a number of filters in the filter bank
		height 		: natural 	:= integer(log2(real(lengthOfIns)));
		--height of a comprarator tree
		heightCompar 		: natural 	:= integer(log2(real(height)))-1
	);
	port(
			clk	: in std_logic;
			reset	: in std_logic;
			input   : in SLV_16;
			start   : in std_logic;
			--output	: out ArrSig_t(1 to height)
			output	: out STD_LOGIC_VECTOR(31 downto 0)
	);
end entity;


--	generic (
--
--		--number of elements to convolve (if the number will change, the coefficients have to be regenerated)
--		lengthOfIns 	: natural  	:= 128 ;
--		-- the code is synthesisable, it is only parameter which is calculated
--		height 		: natural 	:= integer(log2(real(lengthOfIns)))-1
--	);
--	port (		
--			clk	: in std_logic;
--			reset	: in std_logic;
--			input  : in SLV_16;
--			output	: out STD_LOGIC_VECTOR(31 downto 0)
--	);

architecture sequential of waveletTransform  is

component arrayOfMultipliers is
	generic (
		noOfInputs 	: natural;--  	:= 128 ;
		height 		: natural --	:= integer(log2(real(noOfInputs)))
	);
	port (		
		clk		: in std_logic;
		reset		: in std_logic;
		inputShiftArr	: in InputArrSig_t(0 to (2**height)-1);
		inputCoeff	: in InputArrSig_t(0 to (2**height)-1);
		outputToAdd	: out ArrSig_t(0 to (2**height)-1)
	);
end component;


component adderTree is
	generic (
		noOfInputs 	: natural  	;--:= 128 ;
		height 		: natural 	;--:= integer(log2(real(noOfInputs)))
		maxCompensateDelay : natural
	);
	port (		
		clk		: in std_logic;
		reset		: in std_logic;
		inputFromMult	: in ArrSig_t(0 to (2**height)-1);
		output		: out std_logic_vector(31 downto 0)
	);
end component;




--the last element is lost and not necessary, only for simplifying the generate statement
signal shiftSignalArray : InputArrSig_t(0 to lengthOfIns);


-- wavelet coefficients - ROM
type wavelet_table is array(0 to lengthOfIns-1) of integer range -255 to 255;
constant C_WAFELET_TABLE  : wavelet_table := (
  	0,   0,   0,   0,   0,   0,   0,   0,  -1,  -1,  -1,  -1,  -1,
        -1,  -1,  -2,  -2,  -2,  -3,  -3,  -3,  -4,  -4,  -4,  -5,  -5,
        -6,  -7,  -7,  -8,  -9, -10, -11, -12, -13, -14, -15, -16, -17,
       -19, -20, -22, -23, -25, -27, -29, -30, -32, -34, -37, -39, -41,
       -43, -46, -48, -51, -53, -55, -58, -60, -63, -65, -68, -70, -73,
       -75, -77, -79, -81, -82, -84, -85, -87, -87, -88, -89, -89, -89,
       -88, -87, -86, -85, -83, -81, -78, -75, -72, -68, -63, -59, -54,
       -48, -42, -36, -29, -22, -15,  -7,   0,   8,  16,  25,  34,  43,
        52,  61,  70,  79,  89,  98, 106, 115, 124, 132, 140, 147, 155,
       161, 168, 174, 179, 184, 188, 191, 194, 197, 198, 199, 200, 199,
       198, 197, 194, 191, 188, 184, 179, 174, 168, 161, 155, 147, 140,
       132, 124, 115, 106,  98,  89,  79,  70,  61,  52,  43,  34,  25,
        16,   8,   0,  -7, -15, -22, -29, -36, -42, -48, -54, -59, -63,
       -68, -72, -75, -78, -81, -83, -85, -86, -87, -88, -89, -89, -89,
       -88, -87, -87, -85, -84, -82, -81, -79, -77, -75, -73, -70, -68,
       -65, -63, -60, -58, -55, -53, -51, -48, -46, -43, -41, -39, -37,
       -34, -32, -30, -29, -27, -25, -23, -22, -20, -19, -17, -16, -15,
       -14, -13, -12, -11, -10,  -9,  -8,  -7,  -7,  -6,  -5,  -5,  -4,
        -4,  -4,  -3,  -3,  -3,  -2,  -2,  -2,  -1,  -1,  -1,  -1,  -1,
        -1,  -1,   0,   0,   0,   0,   0,   0,   0);

--signal coefficientsArray : InputArrSig_t(0 to lengthOfIns-1);



function gen_coeffConn(coefficientsArrayIn: InputArrSig_t; height : integer; heightMax : integer) 
return InputArrSig_t 
is
	--what is the distance between coefficients in the coefficients array
	-- for index one it is 1 
	variable difference_index : integer ;
	--what is the final length of the output array
	variable len_arr : integer ;
	--a container for connections
	variable coefficientsArrayOut : InputArrSig_t(0 to (2**heightMax) / (2**(heightMax - height))-1);
begin
	difference_index := 2**(heightMax - height);
	len_arr := (2**heightMax) / difference_index;
	--a for loop to assing correct coefficients
	for i in 0 to len_arr-1 loop
	      coefficientsArrayOut(i) := coefficientsArrayIn(i*difference_index);
	end loop;
return coefficientsArrayOut;
end function;


--wires between coefficients and adders
--type coefficientsArrayOut_t  is array (0 to (2**(height))-1) of SLV_16;

type coefficientsArrayOut_t_t  is array (0 to height) of InputArrSig_t(0 to (2**(height))-1); --(0 to height-1)
signal coefficientsArrayOut : coefficientsArrayOut_t_t;

--outputs from adders
--type outputsFromAdders_t is array(0 to lengthOfIns-1) of SLV_32;
--signal outputsFromAdders : outputsFromAdders_t(1 to height);
--output from all filters
signal outputArray : ArrSig_t(1 to height);
--max values of filters which are uset to find the best suited filter
signal maxValuesFiltersArray : ArrSig_t(1 to height);

-- comparator tree signals and components
component comparatorMax port(
		Clk,reset 		: in std_logic;	
		IndexIn1	: in std_logic_vector(7 downto 0);
		SignalValueIn1 	: in SLV_32;
		IndexIn2	: in std_logic_vector(7 downto 0);
		SignalValueIn2 	: in SLV_32;
		IndexOut	: out std_logic_vector(7 downto 0);
		SignalValueOut 	: out SLV_32
	);
	end component;


signal ArrFilter_buf : ArrSig_t(0 to (2**(heightCompar+2))-1);
signal ArrInd_buf : InputArrInd_t(0 to (2**(heightCompar+2))-1);


BEGIN

--parametrizing indexes of coefficients for each filter
process(reset)
  begin
if reset = '1' then
	-- for each bank...
  for h in 1 to height loop
	-- assign only the necessary coefficients
	for i in 0 to ((2**height) / 2**(height - h))-1 loop
	      coefficientsArrayOut(h)(i) <= std_logic_vector(to_signed(C_WAFELET_TABLE(i*2**(height - h)), SLV_16'length));
	end loop;
  end loop;
end if;
end process;


LenWavelet : for el in 1 to height generate
signal multAdd : ArrSig_t(0 to (2**(el))-1);
--signal coefficientsArrayOut : ArrSig_t(0 to (2**(el))-1);--InputArrSig_t(0 to (2**heightMax) / (2**(heightMax - height))-1)
begin
	the_arrayOfMultipliers : arrayOfMultipliers
		generic map(
			noOfInputs  	=> 	2**el,
			height 		=> 	el --integer(log2(real(2**el)))
			--delay for the filters to be compensated
			
		)
		port map(
			clk 		      =>	clk,
			reset		      =>  	reset,
			inputShiftArr	      => 	shiftSignalArray(0 to (2**el) - 1 ),
			inputCoeff            =>  	coefficientsArrayOut(el)(0 to (2**el)-1)	,--(0 to ((2**height) / 2**(height - el))-1),--gen_coeffConn(coefficientsArray, el, height),--((others=> (others=>'0'))),--gen_coeffConn(coefficientsArray, el, height),
			outputToAdd	      =>  	multAdd
		);

	the_adderTree : adderTree
		generic map(
			noOfInputs  	=> 2**el,
			height 		=> el, --integer(log2(real(2**el)))
			maxCompensateDelay =>  2**(height-2)
		)
		port map(
			clk 		=>	clk,
			reset		=>  	reset,
			inputFromMult	=> 	multAdd,
			output		=>  	outputArray(el)
		);

end generate LenWavelet;




--delay and add lines
--shiftSignalArray(0)<=input;

--shift input signal
Registers: process (clk,reset)
begin
	if reset = '1' then
		shiftSignalArray <= ((others=> (others=>'0')));
	elsif Falling_edge(clk) then
		--shiftSignalArray(0)<= input;
--		for el in lengthOfIns-1 downto 0 loop
--			--loop through each tap
--			
--		end loop;
		shiftSignalArray(0 to lengthOfIns-1) <= input & shiftSignalArray(0 to lengthOfIns-2);
--shiftSignalArray(1 to lengthOfIns-1) <= shiftSignalArray(0 to lengthOfIns-2);
	end if;
end process;



--initializing coefficients 
--process (clk,reset)
--  begin
--    if reset = '1' then
--	--write a for loop instead, it has to iterate from -x/2 to x/2, where x is timeline of a wavelet
--	for t in 0 to lengthOfIns-1 loop 
--		coefficientsArray(t) <= std_logic_vector(to_signed(C_WAFELET_TABLE(t), SLV_16'length));

----coefficientsArray(t+(lengthOfIns-1)/2) <= std_logic_vector(to_unsigned(2, SLV_16'length));
--	end loop;
--    end if;
--end process;

--outputArray - output from the bank of filters - many vectors
--output - output from waveletTransform module - only one vector
--maxValuesFiltersArray - max values of filters which are uset to find the best suiting filter
--output<=outputArray;


process(clk,reset,start)
variable outputArrayInternal : ArrSig_t(1 to height);
  begin
	if Rising_edge(clk) then 
		if reset = '1' then
			output <= outputArrayInternal(1);
			outputArrayInternal:=(others => std_logic_vector(to_unsigned(1, SLV_32'length)));
		else


		--driving index of filter bank - ArrInd_buf(0) is index of the choosen filter
		output<=outputArray(to_integer(unsigned(ArrInd_buf(0))+1));
		--if no start just update
		  for h in 1 to height loop 
			--if the output
		  	if signed(outputArrayInternal(h)) < signed(outputArray(h)) then
				outputArrayInternal(h):=outputArray(h);
			end if;
		  end loop;
		--if "start" move the max values to comparator tree to find the index of the best suited filter
		  if start='1' then
			maxValuesFiltersArray<=outputArrayInternal;
			-- reset values 
			outputArrayInternal:=(others => std_logic_vector(to_unsigned(1, SLV_32'length)));
		  else 
			null;			
		  end if;

		end if;

	end if;


end process;


--process(clk,reset,start)
--variable outputArrayInternal : ArrSig_t(1 to height);
--  begin
--	--output<=outputArray(1);
--	--array carrying information about max number of 
--	--outputArrayInternal:=outputArray;
--	if reset = '1' then
--		output <= outputArrayInternal(1);
--		outputArrayInternal:=(others => std_logic_vector(to_unsigned(1, SLV_32'length)));
--	elsif Falling_edge(clk) then
--		--driving index of filter bank - ArrInd_buf(0) is index of the choosen filter
--		output<=outputArray(to_integer(unsigned(ArrInd_buf(0))+1));
--		--if no start just update
--		  for h in 1 to height loop 
--			--if the output
--		  	if signed(outputArrayInternal(h)) < signed(outputArray(h)) then
--				outputArrayInternal(h):=outputArray(h);
--			end if;
--		  end loop;
--		--if "start" move the max values to comparator tree to find the index of the best suited filter
--		if start='1' then
--			maxValuesFiltersArray<=outputArrayInternal;
--			-- reset values 
--			outputArrayInternal:=(others => std_logic_vector(to_unsigned(1, SLV_32'length)));
--		end if;
--
--	end if;  
--end process;





--building comparator tree to find index of the most suiting filter
--SignalValueOut_t<=ArrFilter_buf(0);


the_comp_first:comparatorMax
port map(
SignalValueIn1=> ArrFilter_buf(1),
IndexIn1=>ArrInd_buf(1),
SignalValueIn2=> ArrFilter_buf(2),
IndexIn2=>ArrInd_buf(2),
SignalValueOut=> ArrFilter_buf(0),
IndexOut=>ArrInd_buf(0),
Clk => clk,
reset => reset
);


	--SignalValueOut_t<=ArrFilter_buf(0)
	levels : for k in 1 to heightCompar generate
		buf_array_array : for el in 0 to 2**k-1 generate
				the_comp:comparatorMax
				port map(
				SignalValueIn1=> ArrFilter_buf(2**(k+1)-1 + 2 * el),
				IndexIn1=>ArrInd_buf(2**(k+1)-1 + 2 * el),
				SignalValueIn2=> ArrFilter_buf(2**(k+1) + 2 * el),
				IndexIn2=>ArrInd_buf(2**(k+1) + 2 * el),
				SignalValueOut=> ArrFilter_buf(2**k + el - 1),
				IndexOut=>ArrInd_buf(2**k + el - 1),
				Clk => clk,
				reset => reset
					);
		end generate buf_array_array;
	end generate levels;

--assigning the max values from previous iteration
ArrFilter_buf(((2**(heightCompar+2))-1 ) - 2*(2**heightCompar) to (2**(heightCompar+2))-2) <= maxValuesFiltersArray;


indices : for k in ((2**(heightCompar+2))-1 ) - 2*(2**heightCompar) to (2**(heightCompar+2))-2 generate
--variable index		: natural := 0;
begin
-- +1 because the range of filters is 1 to 8
ArrInd_buf(k)<=std_logic_vector(to_unsigned(k - (((2**(heightCompar+2))-1 ) - 2*(2**heightCompar)), SLV_8'length));
end generate indices;



end architecture;
