
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity comparatorMax is
	port(
		Clk		: in std_logic;
		reset		: in std_logic;
		IndexIn1	: in std_logic_vector(7 downto 0);
		SignalValueIn1 	: in std_logic_vector(31 downto 0);
		IndexIn2	: in std_logic_vector(7 downto 0);
		SignalValueIn2 	: in std_logic_vector(31 downto 0);
		IndexOut	: out std_logic_vector(7 downto 0);
		SignalValueOut 	: out std_logic_vector(31 downto 0)
		);
end entity comparatorMax;

architecture RTL_sequential of comparatorMax is

begin

process(Clk,reset,SignalValueIn1,SignalValueIn2)
	begin
	if Rising_edge(Clk) then
  		if(reset = '1')then
			IndexOut<=std_logic_vector(to_unsigned(1, IndexIn1'length));
			SignalValueOut<=std_logic_vector(to_unsigned(0, SignalValueIn1'length));
  		else
			if unsigned(SignalValueIn1)<unsigned(SignalValueIn2) then
				IndexOut<=IndexIn2;
				SignalValueOut<=SignalValueIn2;
			else
				IndexOut<=IndexIn1;
				SignalValueOut<=SignalValueIn1;
			end if;
  		end if;
	end if;
end process;




end;

