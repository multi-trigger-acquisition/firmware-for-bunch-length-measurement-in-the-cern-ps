library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.Numeric_std.all;
entity Shift_diff_TB is
end;

--testing the result of derivative
--

architecture TB1 of Shift_diff_TB is
  signal Clock, Reset, Enable     		: STD_LOGIC;
  signal DataIn					: STD_LOGIC_VECTOR (15 downto 0);
  signal Expected_DataOut,DataOut		: STD_LOGIC_VECTOR (16 downto 0);
  signal OK            				: Boolean;
begin

  uut: entity work.Shift_diff
  port map (
    Clock 	 => Clock,
    Reset        => Reset,
    Enable       => Enable,
    DataIn       => DataIn,
    DataOut	 => DataOut
  );


  Clk: process
  begin
    while now <= 3000 NS loop
      Clock <= '0';
      wait for 5 NS;
      Clock <= '1';
      wait for 5 NS;
    end loop;
    wait;
  end process;

  process
  begin

    Reset<='0';Enable<='0';DataIn<=STD_LOGIC_VECTOR(to_signed(0,DataIn'length));   Expected_DataOut<=(others => 'X')
;wait for 10 ns;
if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
    --reset and initialise registers
    Reset<='1';Enable<='0';DataIn<=STD_LOGIC_VECTOR(to_signed(0,DataIn'length));   Expected_DataOut<=(others => '0');
wait for 10 ns;
if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
    Reset<='0';Enable<='0';DataIn<=(others => '-');				     Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(0,DataOut'length));
wait for 10 ns;
if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
    Reset<='0';Enable<='0';DataIn<=STD_LOGIC_VECTOR(to_signed(3400,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(0,DataOut'length));
wait for 10 ns;
if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
    Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(3500,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(500,DataOut'length));
wait for 10 ns;
if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
    Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(3700,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(200,DataOut'length));
wait for 10 ns;
if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
    Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(3900,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(200,DataOut'length));
wait for 10 ns;
if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
    Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(4100,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(200,DataOut'length));
wait for 10 ns;
if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
    Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(3900,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(-200,DataOut'length));
wait for 10 ns;
if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
    Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(3800,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(-100,DataOut'length));
wait for 10 ns;
if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
    Reset<='0';Enable<='1';DataIn<=STD_LOGIC_VECTOR(to_signed(3700,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(-100,DataOut'length));
wait for 10 ns;
if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;

    Reset<='0';Enable<='0';DataIn<=STD_LOGIC_VECTOR(to_signed(3300,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(-100,DataOut'length));
wait for 10 ns;
if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;

    wait;
  end process;


end;
