import ShellTypes::*;
import SL3Types::*;

module Accelerator
(
	// clock and reset
	input                               clock_a,
	input                               reset_n_a, 
	//fifo to accelerator
	input PCIEPacket                    data_a,
	input                               empty_a,
	//signal to read from input fifo
	output   reg                          rdreq_a,
	//accelerator to fifo
	output PCIEPacket                   q_a,
	input                               full_a,
	//signal to write to output fifo
	output    reg                        wrreq_a
);    

//reg rdreq_a;
//reg wrreq_a;

//wire       	rdreq_a_signal;
//wire       	wrreq_a_signal;
PCIEPacket 	in_a_signal;
PCIEPacket 	out_a_signal;


typedef enum logic [2:0]{ IDLE, READ, OPERATION, WRITE } State;
State currentState, nextState;
//on the rising edge of the clock
always_ff @(posedge clock_a)
	//check if reset go high then go to IDLE
	if(~reset_n_a) begin
		currentState <= IDLE;
		//in_a_signal <= '{valid: 0, data: '0, slot: '0, pad: '0, last: '0};
		//out_a_signal <= '{valid: 0, data: '0, slot: '0, pad: '0, last: '0};
	//otherwise
	end	
	else 		
		currentState <= nextState;
//combinational logics
always_comb

	case(currentState)
	IDLE: 		if(empty_a == 1) begin	
			nextState = IDLE;
			wrreq_a <= 0;
			rdreq_a <= 0;
			//reset register
			in_a_signal<='{valid: 0, data: '0, slot: '0, pad: '0, last: '0};
			//reset output
			q_a <= '{valid: 0, data: '0, slot: '0, pad: '0, last: '0};
			end
			else begin
			nextState = OPERATION;
			rdreq_a <= 1;
			wrreq_a <= 0;
			//assign data to intermediate value (register)
			in_a_signal <= data_a;
			q_a <= '{valid: 0, data: '0, slot: '0, pad: '0, last: '0};
			end
	OPERATION:	begin 	
			nextState = WRITE;
			wrreq_a <= 0 ;
			rdreq_a <= 0;
			//perform operation
			//in_a_signal.data <= in_a_signal.data + 2;
			q_a <= '{valid: 0, data: '0, slot: '0, pad: '0, last: '0};
			end
	WRITE: 		begin
			nextState = IDLE;
			wrreq_a <= 1 ;
			rdreq_a <= 0;
			q_a <= in_a_signal;
			end
	default: 	nextState = IDLE;
	endcase

	
	
endmodule

