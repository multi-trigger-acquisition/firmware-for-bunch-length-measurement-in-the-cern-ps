library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.sort_pkg.all;

entity maxUpdater is
	generic (
		delayOfFilters 	 : natural  := 128);
	port (		
			Clock,Reset,ResetMaxValue: in std_logic;
			signalIn: in SLV_16;
			timeStampIn: in SLV_16;
			MaxValueOut: out SLV_16;
			timeStampOut: out SLV_16
	);
end entity;
architecture sequential of maxUpdater  is

signal maxValue 	: 	SLV_16;
--signal timeStamp 	: 	SLV_16;
signal resetCondition	:	std_logic;

begin
MaxValueOut<=maxValue;
--timeStampOut<=timeStamp;
-- process responsible for finding max value

resetCondition <= ResetMaxValue or Reset;

process(Clock,resetCondition)
   begin
	if Rising_edge(Clock) then
		if resetCondition = '1' then
			maxValue <= (others => '0');
			timeStampOut <= (others => '0');
		else
			if unsigned(maxValue)<unsigned(signalIn) then
	 			maxValue <=signalIn; 
				timeStampOut <=std_logic_vector(unsigned(timeStampIn)+to_unsigned(delayOfFilters,timeStampOut 'length)); 
			else
				null;
			end if;
		end if;
	end if;
end process;

end architecture;
