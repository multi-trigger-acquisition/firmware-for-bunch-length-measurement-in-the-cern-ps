library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_std.all;
use work.sort_pkg.all;

ENTITY topAccMeas IS
	GENERIC (
		sizeOfWindowTop 	 : natural  := 128;
		--should be twice as much as size of window
		sizeOfFrameTop	 	: natural  := 256;
		thresholdTop	 	: natural  := 0);
      	PORT(   Clock, Reset	 	:	IN std_logic;
               	start	        	: 	IN std_logic;
	       	SignalIn		: 	IN SLV_16;
	       	accurateLength		: 	OUT SLV_16;
	       	validLength	 	:	OUT std_logic
);
END topAccMeas;

ARCHITECTURE TOP_RTL OF topAccMeas IS

COMPONENT serializeCompFSM
        generic (
		sizeOfWindow 	 	: natural  := sizeOfWindowTop;
		--should be twice as much as size of window
		sizeOfFrame 	 	: natural  := sizeOfFrameTop;
		threshold 	 	: natural  := thresholdTop);
	port(
		SignalIn	 	: 	IN SLV_16;
		reset,clk,start	 	: 	IN std_logic;
		accurateLength		: 	OUT SLV_16;
	        validLength	 	:	OUT std_logic
		);
END COMPONENT;


BEGIN

--place for connecting wavelet transform

--downsampling the derivation and detection block
serializeCompFSM_module : serializeCompFSM
PORT MAP (clk=>Clock, reset=>Reset, SignalIn=> SignalIn, start=>start , accurateLength => accurateLength, validLength => validLength);


END TOP_RTL;
