library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.sort_pkg.all;


--this module simply center the window on the max point of the bunch / pulse
Entity WindowCentering is
	generic (
		--it is no more size of window but mitigation of the delay of filters
		sizeOfWindow 	 : natural  := 512);
	port(
		clk,reset,start	 : in std_logic;
		timeStampMax	 : in SLV_16;
		timeCurrent 	 : in SLV_16;
		outUp	 	 : out std_logic
		);
end entity WindowCentering;
architecture sequential of WindowCentering is
signal tempValue		: unsigned(15 downto 0);
--signal timeCurrentReg		: SLV_16;
--signal timeStampMaxReg		: SLV_16;
signal timeToShift		: unsigned(15 downto 0);
signal resetCnt			: std_logic;
begin


process(clk,reset,start)
   begin
	if reset='1'then
		timeToShift <= (others => '0');
		resetCnt<='1';
      	elsif(Rising_edge(clk)) then
		if(start='1') then
			timeToShift<=sizeOfWindow+unsigned(timeStampMax)-unsigned(timeCurrent);
			resetCnt<='1';
		else
			resetCnt<='0';
		end if;
      	end if;
end process;

-- counter for shifting window in time
process(clk,resetCnt)
   begin
	
	if resetCnt='1'then
         	tempValue <= to_unsigned(1,tempValue'length);
      	elsif(Rising_edge(clk)) then
		outUp<='0';
		if(tempValue<timeToShift) then
			tempValue <= tempValue + 1;
		elsif(tempValue=timeToShift) then
			tempValue <= tempValue + 1;
			outUp<='1';
		--else off
		end if;	
      	end if;
end process;





end architecture sequential;
