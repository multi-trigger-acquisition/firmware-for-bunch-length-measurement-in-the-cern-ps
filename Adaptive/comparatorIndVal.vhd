library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity CompIndVal is
	port(
		Clk		: in std_logic;
		IndexIn1	: in std_logic_vector(7 downto 0);
		SignalValueIn1 	: in std_logic_vector(15 downto 0);
		IndexIn2	: in std_logic_vector(7 downto 0);
		SignalValueIn2 	: in std_logic_vector(15 downto 0);
		IndexOut	: out std_logic_vector(7 downto 0);
		SignalValueOut 	: out std_logic_vector(15 downto 0)
		);
end entity CompIndVal;

architecture RTL_sequential of CompIndVal is

begin

process(Clk,SignalValueIn1,SignalValueIn2)
	begin
	if Rising_edge(Clk) then
		if signed(SignalValueIn1)>signed(SignalValueIn2) then
			IndexOut<=IndexIn2;
			SignalValueOut<=SignalValueIn2;
		else
			IndexOut<=IndexIn1;
			SignalValueOut<=SignalValueIn1;
		end if;
	end if;
end process;

end;


--architecture RTL of CompIndVal is
--
--begin
--
--process(SignalValueIn1,SignalValueIn2)
--	begin
--	if signed(SignalValueIn1)>signed(SignalValueIn2) then
--		--IndexOut<=IndexIn2;
--		SignalValueOut<=SignalValueIn2;
--	else
--		--IndexOut<=IndexIn1;
--		SignalValueOut<=SignalValueIn1;
--	end if;
--end process;
--
--end;
--
