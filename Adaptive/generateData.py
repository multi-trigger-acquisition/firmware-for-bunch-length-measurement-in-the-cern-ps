# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 14:05:43 2018

@author: pakozlow
"""

from numpy import *
import numpy as np
import io as io
import matplotlib.pyplot as plt
from scipy import signal

from io import StringIO  

filename="C2595allb_C40-77-78_MHFB_on_C80-88-89_BU4_3x3.5kV_4_1890E10ppp.dat"
data = np.loadtxt(filename, delimiter="\n")

noOfBunches=100
bunchLen=8000
dataRes=np.reshape(data,(noOfBunches,bunchLen))
#shift baseline up
baseline = np.min(dataRes)
dataRes = -baseline+dataRes
#normalize 0-1
dataRes = dataRes/np.max(dataRes)
#max range on 16 bits - 65536 
dataRes = 65535 * dataRes# Now scale by 255
data16 = dataRes.astype(np.uint16)



dataFinal = np.array(data16[0])
plt.figure()
plt.plot(dataFinal, '-')
plt.xlabel('t [ns]')
plt.ylabel('grad(Signal)')
plt.show()

f = open("signalForTB.txt", "w+", encoding="utf-8")
#write noOfBunches
#write start and stop points of the bunches
f.write("%d " %noOfBunches)
f.write("%d\n" %bunchLen)
#writing waveform
for i in dataFinal:
     f.write("%d\n" % (i+1))
f.close()
