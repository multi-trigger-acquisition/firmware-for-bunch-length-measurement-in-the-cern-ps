library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_std.all;
use work.sort_pkg.all;

ENTITY topDesign IS

      	PORT(   Clock, Reset, Enable 	:	IN std_logic;
	       	DataIn			: 	IN SLV_16;
		apprxLen		:	OUT SLV_16;
		apprxLenValid		:	OUT std_logic;
		maxVal			:	OUT SLV_16;
		accurLen		:	OUT SLV_16;
		accurLenValid		:	OUT std_logic
);
END topDesign;

ARCHITECTURE TOP_RTL OF topDesign IS

COMPONENT topDerivThres IS
      PORT(    Clock, Reset, Enable 	:	IN std_logic;
               DataIn        		: 	IN SLV_16;
               DataOut        		:	OUT SLV_16;
	       MaxValueOut		:	OUT SLV_16;
	       startFineMeas		:       OUT std_logic;
	       OutValid 		:	OUT std_logic
);
END COMPONENT;

COMPONENT topAccMeas
        GENERIC (
		sizeOfWindowTop 	 : natural  := 128;
		--should be twice as much as size of window
		sizeOfFrameTop	 	: natural  := 256;
		thresholdTop	 	: natural  := 9000);
      	PORT(   Clock, Reset	 	:	IN std_logic;
               	start	        	: 	IN std_logic;
	       	SignalIn		: 	IN SLV_16;
	       	accurateLength		: 	OUT SLV_16;
	       	validLength	 	:	OUT std_logic
	);
END COMPONENT;

signal startAccMeas 	: 	std_logic;

BEGIN

--place for connecting wavelet transform

--downsampling the derivation and detection block
ThresholdDetection : topDerivThres
PORT MAP (Clock	=> Clock, 
Reset		=> Reset, 
Enable		=> Enable, 
DataIn		=> DataIn, 
DataOut		=> apprxLen, 
MaxValueOut 	=> maxVal,
startFineMeas 	=> startAccMeas,
OutValid 	=> apprxLenValid);

AccurateMeasurement : topAccMeas
PORT MAP (Clock	=> Clock, 
Reset		=> Reset, 
start		=> startAccMeas, 
SignalIn	=> DataIn, 
accurateLength  => accurLen, 
validLength 	=> accurLenValid);

END TOP_RTL;
