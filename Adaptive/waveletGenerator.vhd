-- VHDL 2008
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.sort_pkg.all;
use IEEE.math_real."log2";
use IEEE.MATH_REAL;
use STD.TEXTIO.all;
-- VHDL2008
--	subtype SLV_8  is STD_LOGIC_VECTOR(7 downto 0);        -- define a Byte
--	subtype SLV_16  is STD_LOGIC_VECTOR(15 downto 0);        -- define a Byte
--	type    InputArrInd_t is array(NATURAL range <>) of SLV_8;  -- define a new unconstrained vector of Bytes.
--        type    InputArrSig_t is array(NATURAL range <>) of SLV_16;

entity waveletGeneratorFilter is
	generic (

		--number of elements to convolve (if the number will change, the coefficients have to be regenerated)
		lengthOfIns 	: natural  	:= 512 ;
		-- the code is synthesisable, it is only parameter which is calculated
		height 		: natural 	:= integer(log2(real(lengthOfIns)))-1
	);
	port (		
			clk	: in std_logic;
			reset	: in std_logic;
			input  : in SLV_16;
			start : in std_logic;
			output	: out STD_LOGIC_VECTOR(31 downto 0);
			accurateLength		: 	in SLV_16;
	        	validLength	 	:	in std_logic
			
	);
end entity;

architecture sequential of waveletGeneratorFilter  is

component Tap
port (		
	Clock		: in std_logic;
	signalIn	: in SLV_16;
	coefficient	: in SLV_16;
	signalOut	: out SLV_16;
	multResult	: out STD_LOGIC_VECTOR(31 downto 0)
);
end component;

signal coefficientsArray : InputArrSig_t(0 to lengthOfIns-1);

--the last element is lost and not necessary, only for simplicity of generate statement
signal shiftSignalArray : InputArrSig_t(0 to lengthOfIns);
type    multResArray_t is array(NATURAL range <>) of STD_LOGIC_VECTOR(31 downto 0);
signal multResArray 	: multResArray_t(0 to lengthOfIns-1);

--array of connections in the adder tree
signal ArrSig_buf 	: multResArray_t(0 to (2**(height+2))-1);

--subtype word_t is std_logic_vector(15 downto 0);
--type    wavelet_t   is array(0 to 512 - 1) of word_t;
type    ram_t   is array(0 to 230 - 1) of InputArrSig_t(0 to lengthOfIns-1);

-- function to return an initialization vector
-- Using file I/O violates the 'pure rule' for functions in VHDL. 
-- So impure is needed.
impure function ocram_ReadMemFile return ram_t is
  file FileHandle       : TEXT open READ_MODE is "coefficentsRAM.txt";
  variable CurrentLine  : LINE;
  variable Result       : ram_t    := (others => (others => "0000000000000000" ));
  variable temp 	: Integer;

begin

  --readline(FileHandle, CurrentLine);
  --READ(CurrentLine, noOfWavel);

  for i in 0 to 230 - 1 loop
    for y in 0 to 512 - 1 loop
	readline(FileHandle, CurrentLine);
  	READ(CurrentLine, temp);
    	Result(i)(y) := std_logic_vector(to_signed(temp,16));
	end loop;
  end loop;


  return Result;
end function;

signal ram    : ram_t    := ocram_ReadMemFile;
signal index : unsigned(15 downto 0);
signal indexT : unsigned(31 downto 0);

-- wavelet coefficients - ROM
--signal C_WAFELET_TABLE  : wavelet_t := ram(100);


begin

-- Td = accurateLength	
--process (clk, reset, accurateLength, validLength)
--begin
--  if reset = '0' then
--    -- seed length
--	--for t in 0 to lengthOfIns-1 loop
--		index<=to_unsigned(20,index'length);
--		
--	--end loop;
--  else
--    if RISING_EDGE(clk) and validLength='1' then
--	--if(validLength='1' and to_integer(unsigned(accurateLength))<230) then
--	--for t in 0 to lengthOfIns-1 loop 
--		index <= unsigned(accurateLength);
--	--end loop;
--	--end if;
--    end if;
--  end if;
--end process;



process(clk,reset,validLength)
   begin
	if reset='1' then
		index<=to_unsigned(100,index'length);
      	elsif(Rising_edge(clk) and validLength='1' and unsigned(accurateLength)<229 and unsigned(accurateLength)>5) then
		indexT <= (((unsigned(accurateLength))*1000)/1732);
		index <=  resize( unsigned( indexT ), 16 ); 
      	end if;
end process;




coefficientsArray <= ram(to_integer(index));
--delay and add lines
shiftSignalArray(0)<=input;
conv_filter : for tapEl in 0 to lengthOfIns-1 generate
		the_tap:Tap
		port map(
			Clock 		=>	clk,
			signalIn	=>  	shiftSignalArray(tapEl),
			coefficient	=> 	coefficientsArray(tapEl) ,
			signalOut	=>  	shiftSignalArray(tapEl+1),
			multResult	=>  	multResArray(tapEl)
			);
end generate conv_filter;

-- A simple adder tree, analogous situation like in the MaxMinFind module
process(clk,reset)
  begin
	if reset = '1' then
	--initial values for the array of connections
	for k in 0 to (2**(height+2))-1 loop
		ArrSig_buf(k)<= std_logic_vector(to_signed(0, 32));
	end loop;
	output<=ArrSig_buf(0);
	else 
	for k in 0 to height loop
		for el in 0 to 2**k-1 loop
			--loop through each adder
			ArrSig_buf(2**k + el - 1) <= std_logic_vector(
			signed(ArrSig_buf(2**(k+1)-1 + 2 * el)) + 
			signed(ArrSig_buf(2**(k+1) + 2 * el))
			);
		end loop;
	end loop;
	output<=ArrSig_buf(0);
	ArrSig_buf(((2**(height+2))-1 ) - 2*(2**height) to (2**(height+2))-2) <= multResArray;
    end if;
end process;




end architecture;

--##generation of seed coefficients in python
--def ricker(Td, length=100):
--     f=(sqrt(6))/(np.pi*Td)
--     fromto = int(floor(length/2))
--     y = np.array([0.0]*length)
--     for t in range(-fromto, fromto):
--#         y[t+49] = (1/(10**4)) * (1/(10**3)) * (10**3 - (1199716128/(Td**2)) * (1/(10**5)) * t**2 )  * \
--#         ( 10**4    +     (599858064/(Td**2) * (1/(10**4)) * t**2 )    +    ((599858064 * (1/(10**6)) *t**2)/(Td**2))/2 + \
--#         ((10 * ( ( 599858064 * (1/(10**7)) * t**2 ) / (Td**2) ))**3)/6 + (( ( 599858064 * (1/(10**7)) * t**2 ) / (Td**2) )**4)/24)
--         y[t+fromto]=(1.0 - 2.0*(np.pi**2)*(f**2)*(t**2)) * np.exp(-(np.pi**2)*(f**2)*(t**2))
--     return y
-- 
--#creating a look-up table for wavelet coefficients
--fromTd=0
--ToTd=230
--noOfWavel=ToTd-fromTd
--arrayOfCorrespondingTd = np.array([0]*noOfWavel)
--arrayOfCoefficients = np.array([np.array([0]*512)]*(noOfWavel))
--for x in range(fromTd+1, ToTd):
--    arrayOfCoefficients[x-fromTd] = (ricker(x,512)*100).astype(int)
--    arrayOfCorrespondingTd[x-fromTd] = x
--    
--fig2 = plt.figure()
--ax3 = fig2.add_subplot(1, 1, 1)
--#print(inputSignal)
--i3 = ax3.imshow(arrayOfCoefficients, cmap="hot", aspect='auto',origin="lower")
--ax3.set_xlabel('No. of sample')
--ax3.set_ylabel('No. of frame')
--fig2.colorbar(i3)  
--
--
--#save
--f = open("coefficentsRAM.txt", "w+", encoding="utf-8")
--#write noOfBunches
--#write start and stop points of the bunches
--f.write("%d\n" %noOfWavel)
--#writing waveform
--for x in arrayOfCoefficients:
--    for i in x:
--        f.write("%d\n" % (i+1))
--f.close()