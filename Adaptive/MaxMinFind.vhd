--this package is used only as a scope for testbench
library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_std.all;

--scope to internal signals
package SpyOnMySigPkgMaxMinFind is
  -- synthesis translate_off   
	signal validInGlobal	:  std_logic;
  -- synthesis translate_on 
end package SpyOnMySigPkgMaxMinFind ;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--The module cositsts of a tree of comparators.
--The lowest difference between threshold and signal will indicate the moment of the intersection.
package sort_pkg is
	subtype SLV_8  is STD_LOGIC_VECTOR(7 downto 0);        -- define a Byte
	subtype SLV_16  is STD_LOGIC_VECTOR(15 downto 0);        -- define a Byte
	type    InputArrInd_t is array(NATURAL range <>) of SLV_8;  -- define a new unconstrained vector of Bytes.
    	type    InputArrSig_t is array(NATURAL range <>) of SLV_16;
    	subtype SLV_32  is STD_LOGIC_VECTOR(31 downto 0);
    	type ArrSig_t is array(NATURAL range <>) of SLV_32;
    	type ArrSig_t_t is array(NATURAL range <>) of ArrSig_t;
		
end package;



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.sort_pkg.all;
--for TB*
use work.SpyOnMySigPkgMaxMinFind.all;

Entity MaxMinFind is
	--generic height of the tree
	generic (
		height : natural  := 6);
	port(
		clk		 : in std_logic;
		validIn		 : in std_logic;
		InputArrSig	 : in InputArrSig_t(0 to 2*(2**height)-1);
		SignalValueOut_t : out SLV_16;
		validOut	 : out std_logic;
		IndexOut_t	 : out std_logic_vector(7 downto 0)
		);
end entity MaxMinFind;


architecture repetitive_sequential of MaxMinFind is

	component CompIndVal port(
		Clk 		: in std_logic;	
		IndexIn1	: in std_logic_vector(7 downto 0);
		SignalValueIn1 	: in SLV_16;
		IndexIn2	: in std_logic_vector(7 downto 0);
		SignalValueIn2 	: in SLV_16;
		IndexOut	: out std_logic_vector(7 downto 0);
		SignalValueOut 	: out SLV_16
	);
	end component;


signal ArrSig_buf : InputArrSig_t(0 to (2**(height+2))-1);
signal ArrInd_buf : InputArrInd_t(0 to (2**(height+2))-1);

--array which elements correspond to pipeline stages - if true, then
--the pipeline stage is valid/contains useful data 
signal validPLStage : std_logic_vector(height-1 downto 0);

begin
--the signel is used only for tests in TB 
validInGlobal<=validIn;


--pipeline valid stage process
Registers: process (clk)
begin
	if Rising_edge(clk) then
		validPLStage(height-1 downto 1) <= validPLStage(height-2 downto 0);
		validPLStage(0)<=validIn;
		validOut<=validPLStage(height-1);
	end if;
end process;

-- building comparator tree
SignalValueOut_t<=ArrSig_buf(0);
IndexOut_t<=ArrInd_buf(0);

the_comp_first:CompIndVal
port map(
SignalValueIn1=> ArrSig_buf(1),
IndexIn1=>ArrInd_buf(1),
SignalValueIn2=> ArrSig_buf(2),
IndexIn2=>ArrInd_buf(2),
SignalValueOut=> ArrSig_buf(0),
IndexOut=>ArrInd_buf(0),
Clk => clk
);


	--SignalValueOut_t<=ArrSig_buf(0)
	levels : for k in 1 to height generate
		buf_array_array : for el in 0 to 2**k-1 generate
				the_comp:CompIndVal
				port map(
				SignalValueIn1=> ArrSig_buf(2**(k+1)-1 + 2 * el),
				IndexIn1=>ArrInd_buf(2**(k+1)-1 + 2 * el),
				SignalValueIn2=> ArrSig_buf(2**(k+1) + 2 * el),
				IndexIn2=>ArrInd_buf(2**(k+1) + 2 * el),
				SignalValueOut=> ArrSig_buf(2**k + el - 1),
				IndexOut=>ArrInd_buf(2**k + el - 1),
				Clk => clk
					);
		end generate buf_array_array;
	end generate levels;

	ArrSig_buf(((2**(height+2))-1 ) - 2*(2**height) to (2**(height+2))-2) <= InputArrSig;


indices : for k in ((2**(height+2))-1 ) - 2*(2**height) to (2**(height+2))-2 generate
--variable index		: natural := 0;
begin
--index:=index+1;
ArrInd_buf(k)<=std_logic_vector(to_unsigned(k - (((2**(height+2))-1 ) - 2*(2**height)) , SLV_8'length));
--
end generate indices;


end architecture repetitive_sequential;

