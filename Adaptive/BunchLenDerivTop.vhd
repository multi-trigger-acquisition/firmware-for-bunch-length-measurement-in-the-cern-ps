library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_std.all;
use work.sort_pkg.all;

ENTITY topDerivThres IS
      PORT(    Clock, Reset, Enable 	:	IN std_logic;
               DataIn        		: 	IN SLV_16;
               DataOut        		:	OUT SLV_16;
	       MaxValueOut		:	OUT SLV_16;
	       startFineMeas		:       OUT std_logic;
	       OutValid 		:	OUT std_logic
);
END topDerivThres;

ARCHITECTURE TOP_RTL OF topDerivThres IS

COMPONENT Shift_diff
        PORT(	Clock, Reset, Enable	: 	in Std_logic;
		ValidOut		: 	out Std_logic;
        	DataIn			: 	in SLV_16;
        	DataOut			: 	out Std_logic_vector(16 downto 0));
END COMPONENT;

COMPONENT FSM_ThresDetec
        PORT(	Clock, Reset, InValid 	: 	in STD_LOGIC;
		DataIn			: 	in Std_logic_vector(16 downto 0);
		OutValid, ResetMaxValue	: 	out STD_LOGIC;
		DataOut			: 	out SLV_16;
		timestampMax 		: 	out SLV_16
);
END COMPONENT;

COMPONENT DownsamplingInterf
	port (	Clock, Reset		: 	in Std_logic;
		ClockOut		: 	out Std_logic);
END COMPONENT;

COMPONENT maxUpdater
	generic (
		delayOfFilters 	 : natural  := 128);
	port (	Clock,Reset		: 	in std_logic;
		ResetMaxValue		: 	in std_logic;
		signalIn		: 	in SLV_16;
		timeStampIn		: 	in SLV_16;
		MaxValueOut		: 	out SLV_16;
		timeStampOut		: 	out SLV_16
);
END COMPONENT;

COMPONENT WindowCentering
	generic (
		sizeOfWindow 	 : natural  := 256);
	port(
		clk,reset,start	 : in std_logic;
		timeStampMax	 : in SLV_16;
		timeCurrent 	 : in SLV_16;
		outUp	 	 : out std_logic
		);
END COMPONENT;

SIGNAL valid_in_out   			: std_logic;
SIGNAL data				: Std_logic_vector(16 downto 0);
SIGNAL downsampledClock 		: std_logic;

-- DerivThresDetec to MaxUpdater
SIGNAL resetMaxValue_inter		: std_logic;
SIGNAL timestampMax_inter 		: SLV_16;

--DerivThresDetec to WindowCentering
SIGNAL validDetected  			: std_logic;
SIGNAL detectedTSMax 			: SLV_16;


BEGIN

OutValid<=validDetected;

--downsampling the derivation and detection block
DownsamplingInterf_module : DownsamplingInterf
PORT MAP (Clock=>Clock, Reset=>Reset, ClockOut=>downsampledClock );

shift_and_diff_module : Shift_diff 
PORT MAP (Clock=>downsampledClock, 
Reset=>Reset, 
Enable=>Enable, 
DataIn=>DataIn, 
ValidOut=>valid_in_out,
DataOut=>data);

threshold_detector : FSM_ThresDetec
PORT MAP (Clock=>Clock, 
Reset=>Reset,
DataIn=>data, 
InValid=>valid_in_out, 
OutValid=>validDetected,
DataOut=>DataOut, 
ResetMaxValue=>resetMaxValue_inter, 
timestampMax=>timestampMax_inter
);

max_value_update : maxUpdater
GENERIC MAP (
		delayOfFilters => 128)
PORT MAP (
Clock=>Clock,
Reset=>Reset,
ResetMaxValue=>resetMaxValue_inter,
signalIn=>DataIn,
timeStampIn=>timestampMax_inter,
MaxValueOut=>MaxValueOut,
timeStampOut=>detectedTSMax
);


center_the_window : WindowCentering
PORT MAP (
clk=>Clock,
reset=>Reset,
start=>validDetected,
timeStampMax=>detectedTSMax,
timeCurrent=>timestampMax_inter,
outUp=>startFineMeas
);



--
--PROCESS(clock)
--BEGIN
--     IF (clock'event AND clock='1') THEN
--         w_reg<=w_in; x_reg<=x_in; y_reg<=y_in; z_out<=z_reg; 
--END IF; END PROCESS; 

END TOP_RTL;