library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.sort_pkg.all;

ENTITY TB_MaxMin IS
END ENTITY TB_MaxMin;

ARCHITECTURE testbench_1 OF TB_MaxMin IS

constant HEIGHT 			: integer := 7;
signal Clock     			: STD_LOGIC;
signal InputArrSig     			: InputArrSig_t(0 to 2*(2**HEIGHT)-1);
signal SignalValueOut_t 		: SLV_16;
signal IndexOut_t			: SLV_8;
signal validIn				: STD_LOGIC;
signal OK 				: Boolean := True;

BEGIN

DUT: entity  work.MaxMinFind(repetitive_sequential)
GENERIC MAP (height => HEIGHT) 
PORT MAP (clk=>Clock ,
InputArrSig=>InputArrSig ,
SignalValueOut_t=>SignalValueOut_t,
IndexOut_t=>IndexOut_t,
validIn=>validIn); 

--clock
 Clk: process
  begin
    while now <= 100 NS loop
      Clock <= '0';
      wait for 5 NS;
      Clock <= '1';
      wait for 5 NS;
    end loop;
    wait;
  end process;



process
  begin

InputArrSig(0)<=std_logic_vector(to_signed(5,SLV_16'length));
InputArrSig(1)<=std_logic_vector(to_signed(8,SLV_16'length));
InputArrSig(2)<=std_logic_vector(to_signed(5,SLV_16'length));
InputArrSig(3)<=std_logic_vector(to_signed(2,SLV_16'length));
InputArrSig(4)<=std_logic_vector(to_signed(4,SLV_16'length));
InputArrSig(5)<=std_logic_vector(to_signed(9,SLV_16'length));
InputArrSig(6)<=std_logic_vector(to_signed(7,SLV_16'length));
InputArrSig(7)<=std_logic_vector(to_signed(3,SLV_16'length));
InputArrSig(8)<=std_logic_vector(to_signed(5,SLV_16'length));
InputArrSig(9)<=std_logic_vector(to_signed(8,SLV_16'length));
InputArrSig(10)<=std_logic_vector(to_signed(5,SLV_16'length));
InputArrSig(11)<=std_logic_vector(to_signed(2,SLV_16'length));
InputArrSig(12)<=std_logic_vector(to_signed(4,SLV_16'length));
InputArrSig(13)<=std_logic_vector(to_signed(9,SLV_16'length));
InputArrSig(14)<=std_logic_vector(to_signed(7,SLV_16'length));
InputArrSig(15)<=std_logic_vector(to_signed(3,SLV_16'length));
 

wait for 400 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    --reset and initialise registers
--    Reset<='1';Enable<='0';DataIn<=STD_LOGIC_VECTOR(to_signed(0,DataIn'length));   Expected_DataOut<=(others => '0');
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    Reset<='0';Enable<='0';DataIn<=(others => '-');				     Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(0,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--    Reset<='0';Enable<='0';DataIn<=STD_LOGIC_VECTOR(to_signed(3400,DataIn'length));Expected_DataOut<=STD_LOGIC_VECTOR(to_signed(0,DataOut'length));
--wait for 10 ns;
--if(DataOut = Expected_DataOut) then OK <= true;  else OK <= false; end if;
--
    wait;
end process;


END ARCHITECTURE testbench_1;


