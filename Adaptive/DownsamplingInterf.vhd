library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_std.all;

entity DownsamplingInterf is
  port (Clock, Reset: in Std_logic;
	ClockOut: out Std_logic);
end;



architecture RTL of DownsamplingInterf is
	signal count: unsigned(7 downto 0);
	signal maxValue: unsigned(7 downto 0);
	signal outVal: std_logic;

begin

ClockOut<=outVal;
--counter for omitting samples
process(Clock,Reset)
   begin
	if (Rising_edge(Clock)) then
		if Reset='1' then
			count<=(others => '0');
			--to be modified to be generic
			maxValue<=to_unsigned(2,maxValue'length);
			outVal<='0';
		else
			if unsigned(count) < maxValue then
			count	<= 	count + 1;
			else 
			count	<=	to_unsigned(0,count'length);
			outVal  <= not outVal;
			end if; 
		end if;
	end if;
end process;

--process(Clock,Reset)
--   begin
--      if Reset='1' then
--	count<=(others => '0');
--	--to be modified to be generic
--	maxValue<=to_unsigned(2,maxValue'length);
--	outVal<='0';
--      elsif(Falling_edge(Clock)) then
--		if unsigned(count) < maxValue then
--		count	<= 	count + 1;
--		else 
--		count	<=	to_unsigned(0,count'length);
--		outVal  <= not outVal;
--		end if; 
--      end if;
--end process;

end;
