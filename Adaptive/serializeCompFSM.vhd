library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.sort_pkg.all;
use IEEE.math_real."log2";

-- VHDL 2008
--this module simply center the window on the max point of the bunch / pulse
Entity serializeCompFSM is
	generic (
		sizeOfWindow 	 	: natural  := 128;
		--should be twice as much as size of window
		sizeOfFrame 	 	: natural  := 256;
		threshold 	 	: natural  := 0);
	port(
		SignalIn	 	: 	IN SLV_16;
		reset,clk,start	 	: 	IN std_logic;
		accurateLength		: 	OUT SLV_16;
	        validLength	 	:	OUT std_logic
		);
end entity serializeCompFSM;
architecture sequential of serializeCompFSM is

signal ArrSig_buf : InputArrSig_t(0 to sizeOfFrame-1);

type StateType is (ZERO, FIRST_WINDOW, SECOND_WINDOW);
signal State, NextState: StateType;

COMPONENT MaxMinFind
        generic (
		--2**height is the size of the window 2**7 is 128
		height : natural  := 6);
	port(
		clk		 : in std_logic;
		validIn		 : in std_logic;
		InputArrSig	 : in InputArrSig_t(0 to 2*(2**height)-1);
		SignalValueOut_t : out SLV_16;
		validOut	 : out std_logic;
		IndexOut_t	 : out SLV_8
		);
END COMPONENT;

COMPONENT waveletGeneratorFilter
	generic (

		--number of elements to convolve (if the number will change, the coefficients have to be regenerated)
		lengthOfIns 	: natural  	:= 512 ;
		-- the code is synthesisable, it is only parameter which is calculated
		height 		: natural 	:= integer(log2(real(lengthOfIns)))-1
	);
	port (		
			clk			: in std_logic;
			reset			: in std_logic;
			input  			: in SLV_16;
			start 			: in std_logic;
			output			: out STD_LOGIC_VECTOR(31 downto 0);
			accurateLength		: IN SLV_16;
	        	validLength	 	: IN std_logic
	);
END COMPONENT;


--array interconnecting input signal with MUXes to comparator
signal ArrSig_window 	 : InputArrSig_t(0 to sizeOfWindow-1);
signal valueFromCompTree : SLV_16;
signal indexFromCompTree : SLV_8;
signal validIn 		 : std_logic;
signal validOut 	 : std_logic;

signal accurateLengthResult : SLV_16;
signal pastSample : SLV_16;

signal outWavelet	 : std_logic_vector(31 downto 0);
begin


--to test the wavelet it is placed next to max min find module not in between an input and the module.
waveletGeneratorFilter_module : waveletGeneratorFilter
PORT MAP (clk=>clk, 
reset	=> reset,
input   => SignalIn,
output	=> outWavelet,
start => start,
accurateLength => accurateLength,
validLength => validLength
);

MaxMinFind_module : MaxMinFind 
PORT MAP (clk=>clk, 
InputArrSig=>ArrSig_window, 
SignalValueOut_t=>valueFromCompTree, 
IndexOut_t=>indexFromCompTree,
validIn=> validIn,
validOut=> validOut
);

--Output Length: 
--this functionality is simplified
--there will be false positives about valid length
--I've left it like that so that it can be customized
--in the future - to a number of points we want to 
--acquire from one pulse.
accurateLength <= accurateLengthResult;
LengthCalc: process (clk,indexFromCompTree)
begin
	if Rising_edge(clk) then
		pastSample<="00000000" & indexFromCompTree;
		accurateLengthResult <=std_logic_vector((to_unsigned(sizeOfWindow,accurateLengthResult'length) - unsigned(pastSample)) + unsigned(indexFromCompTree));
		--std_logic_vector((unsigned(indexFromCompTree)+ to_unsigned(sizeOfWindow,accurateLengthResult'length)) - unsigned(pastSample) );
		if(validOut='1') then
			validLength <= '1';
		else
			validLength <= '0';
		end if;
	end if;
end process;


--Pipeline input:
process(clk,reset,outWavelet)
   begin
	if reset='1' then
		ArrSig_buf <= ((others => (others => '0')));
      	elsif(Rising_edge(clk)) then
		--shift operation
		for index in sizeOfFrame-1 downto 0 loop
			if(index = 0) then
			--ArrSig_buf(0)<=std_logic_vector(signed(outWavelet)-to_signed(threshold,outWavelet'length))(26 downto 11);
			ArrSig_buf(0)<=outWavelet(26 downto 11);
			else
			ArrSig_buf(index)<=ArrSig_buf(index-1);
			end if;
		end loop;
      	end if;
end process;


--Control the input to comparator tree:
Registers: process (clk,reset)
begin
	if reset = '1' then
		State<=ZERO;
	elsif Falling_edge(clk) then
		State <= NextState;
	end if;
end process;

comp_tree_serial: process (State,start,ArrSig_buf)
begin
validIn<='0';
case State is
when ZERO =>
	ArrSig_window<=((others => (others => '0')));
	if start='1' then
		NextState <= FIRST_WINDOW;
		--find intersection in first window
	else 
		NextState <= ZERO;
	end if;
when FIRST_WINDOW =>
	NextState <= SECOND_WINDOW;
	ArrSig_window <= ArrSig_buf(0 to sizeOfWindow-1);
	--validIn<='1';
		--find intersection in second window
when SECOND_WINDOW =>
	NextState <= ZERO;
	ArrSig_window <= ArrSig_buf(sizeOfWindow to sizeOfFrame-1);
	--validIn for informing that the length is the final one
	validIn<='1';
when others =>
	null;
end case;
end process;

end architecture sequential;